FROM node:14.17.5-alpine

WORKDIR /app
COPY . .
VOLUME /app/files
RUN npm i 
EXPOSE 8445
CMD ["npm", "start"]



