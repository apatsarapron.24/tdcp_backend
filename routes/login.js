/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
const axios = require('axios')
const { Admin, Invoices, Transaction, Invoices_ERP, Report_Problem, Company, Payer } = require('../models/index')
const sequelize = require('sequelize')
const Op = sequelize.Op
const encode_decode = require('./encode_decode')
const authen = require('./authen')
const CryptoJS = require('crypto-js')
const gen = require('./gen')
const fs = require('fs')

router.post('/login_admin', async (req, res, next) => {
  try {
    let { source } = req.body
    let array_source = encode_decode.decode(source)
    let { data: data_login } = await axios.post(process.env.API_ONEID + '/api/oauth/getpwd', {
      grant_type: 'password',
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      username: array_source[0],
      password: array_source[1],
      scope: 'pic'
    })
    let authorization = data_login.token_type+' '+data_login.access_token
    // console.log('authorization')
    // console.log(authorization)    
    let { data: data_account } = await axios.get(process.env.API_ONEID+'/api/account', { 
      params: {}, 
      headers: { 'Authorization': authorization } 
    })
    // console.log("data_account")
    // console.log(data_account)

    let { data: data_pic } = await axios.get(process.env.API_ONEID+'/api/v2/service/citizen/info', { 
      params: {}, 
      headers: { 'Authorization': authorization } 
    })
    // console.log('data_pic')
    // console.log(data_pic.data)
    let token = authen.create_token('')
    let user = await Admin.findOne({ where: { email: data_account.thai_email } })
    if (user) {
      res.send({status: 'Success', token: token, first_name: data_account.first_name_th,
      last_name: data_account.last_name_th, email: data_account.thai_email, createBy: data_account.id+":"+data_account.thai_email,
      company_name_th: 'บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน)',pic: data_pic.data.pic  })
      await user.update({ 
        first_name: data_account.first_name_th,
        last_name: data_account.last_name_th,
        account_id: data_account.id
      })

    } else {
      res.send({ status: 'invalid role' })
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    // res.send({status: 'Error'}) 
    if (error.response.status === 401) {
        res.send({status: 'Invalid Username or Password'})
    }
  }
})


router.post('/login_business', async (req, res, next) => {
  try {
    let { source } = req.body
    // console.log('req.body')
    // console.log(req.body)
    let array_source =  encode_decode.decode(source);
    // console.log('array_source')
    // console.log(array_source)
    let { data: data_login } = await axios.post(process.env.API_ONEID+'/api/oauth/getpwd', {
      grant_type: 'password',
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      username: array_source[0],
      password: array_source[1],
      scope: 'pic'
    })
    let authorization = data_login.token_type+' '+data_login.access_token
    // console.log('authorization')
    // console.log(authorization)    
    let { data: data_account } = await axios.get(process.env.API_ONEID+'/api/account', { 
      params: {}, 
      headers: { 'Authorization': authorization } 
    })
    // console.log("data_account")
    // console.log(data_account)

    let { data: data_pic } = await axios.get(process.env.API_ONEID+'/api/v2/service/citizen/info', { 
      params: {}, 
      headers: { 'Authorization': authorization } 
    })
    // console.log('data_pic')
    // console.log(data_pic.data)

    let hash_citizen_taxid = data_account.hash_id_card_num
    let biz_taxid = array_source[2]
    let hash_biz_taxid = array_source[3]
    
    let { data: data_biz } = await axios.get(process.env.API_ONEID+'/api/service/business/account/'+biz_taxid, { 
      params: {}, 
      headers: { 'Authorization': authorization } 
    })

    if (data_biz.result === 'Success') {
      let { data: data_biz_role } = await axios.get(process.env.API_ONEID+'/api/v2/service/business/role?tax_id='+biz_taxid, { 
        params: {}, 
        headers: { 'Authorization': authorization } 
      })   
      let biz_role_id = ''
      for (let i=0; i<data_biz_role.data.length; i++) {
        if (data_biz_role.data[i].role_name === 'admin-ipay') {
          biz_role_id = data_biz_role.data[i].id
          break;
        }
      }
      if (biz_role_id) {
        let { data: data_biz_roleid } = await axios.get(process.env.API_ONEID+'/api/v2/service/business/role/'+biz_role_id+'?tax_id='+biz_taxid, { 
          params: {}, 
          headers: { 'Authorization': authorization } 
        })   
        let status_biz_role = ''
        for (let i=0; i<data_biz_roleid.data.has_account.length; i++) {
          if (data_biz_roleid.data.has_account[i].account.hash_id_card_num === hash_citizen_taxid) {
              status_biz_role = 'success'
          }
        }
        if (!status_biz_role) { 
          res.send({status: 'Invalid Role'})
        }
        var payer_data = await Payer.findOne({ where: { tax_id: biz_taxid } })
        // console.log('payer_data')
        // console.log(payer_data)
        if (payer_data) {
          await Payer.update({ 
           name_th: data_biz.data[0].name_on_document_th,
           name_eng: data_biz.data[0].name_on_document_eng
          }, { 
          where: { tax_id: biz_taxid }
          })
        } else {
            await Payer.create({
              tax_id: biz_taxid,
              name_th: data_biz.data[0].name_on_document_th,
              name_eng: data_biz.data[0].name_on_document_eng
            })
          // res.send({status: 'Not Found Your Company, Please Contact Administrator'})
        }
          let token = authen.create_token(biz_taxid)
          data_account['token'] = token
          data_account['company_name_th'] = data_biz.data[0].name_on_document_th
          data_account['company_name_eng'] = data_biz.data[0].name_on_document_eng
          res.json({status: 'Success', data: data_account, pic: data_pic.data})                             

      } else {
        res.json({status: 'Invalid Role'})
      }  
    } else {
      res.json({status: 'Invalid Tax ID'})
    }
  } 
  catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    // res.json({status: 'Error'}) 
    if (error.response.status === 401) {
        res.json({status: 'Invalid Username or Password'})
    }
  }
})


module.exports = router