/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Company, Bank, Payment_Tracking, Admin } = require('../models/index')
const Sequelize = require('sequelize')
const db = require('../models')
const { QueryTypes } = require('sequelize')
const Op = Sequelize.Op
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')


router.get('/admin_management', async (req, res, next) => {
  try {
    let token  = req.headers.authorization
    let validate_token = authen.validate_token(token)
    let sender_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401);
    } else {

      var alladmin = await db.sequelize.query(
        "select A.first_name +' '+ A.last_name as name ,A.email ,A.account_id from Admins as A",
        {
          replacements: {},
          type: QueryTypes.SELECT
        }
      )
      res.send(alladmin)
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/admin_add', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
    let email = req.body.email
    var admin_Data = await Admin.findAll({ 
        where: { 
            email: email
        } 
    })    
    if(admin_Data.length == 0){
      await Admin.create({
          email: email,
        })
      res.send('Success')
    }else{
      res.send('Duplicate Email')
    }
  }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/admin_del', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      Admin.destroy({
        where: { email: req.body.email }
      })
      res.send('Success')
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/admin_edit', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      let data_editby = req.body.editBy
      Admin.update(
        {
          email: req.body.email,
          editBy: data_editby
        },
        { where: { email: req.body.past_email } }
      )
      res.send('Success')
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router
