const express = require('express')
const router = express.Router()
const axios = require('axios')
const { Invoices, Transactions, Invoices_ERP, Report_Problem, User, UserLog } = require('../models/index')
const sequelize = require('sequelize')
const Op = sequelize.Op
const encode_decode = require('./encode_decode')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')

router.get('/customer_name/:name', async (req, res, next) => {
  try {
    var { name } = req.params
    name = decodeURI(name)
    let token = req.headers.authorization
    // let name = req.headers.sub
    let validate_token = authen.validate_token(token)
    let id = validate_token.source.id
    let role = validate_token.source.role
    // var invoices = {}
    // res.send({a: decodeURI(name)})
    if (validate_token.status === 'invalid token' || !role) {
      res.send(401)
    } else {
      let invoices = await Invoices_ERP.findOne({
        where: {
          customer_name: {
            [Op.like]: name + '%'
          }
        }
        // order: [['due_date', 'ASC']]
      })
      await UserLog.create({
        search: name,
        searchBy: validate_token.source.id
      })
      // res.send(invoices)
      if (invoices) {
        var invoicesView = {}
        Object.assign(invoicesView, {
          'token': authen.create_token_roleTaxid(id, role, invoices.hash_tax_id),
          'customerName': invoices.customer_name
        })
        res.send(invoicesView)
        // res.send(invoices)
        // console.log(invoices.length)
      } else {
        res.send({ 'status': 'invalid customer name' })
      }
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/report_problem', async (req, res, next) => {
  try {
    let subject = req.body.subject
    let detail = req.body.detail
    let username = req.body.username
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      let reportProblem = await Report_Problem.create({
        subject: subject,
        detail: detail,
        username: username
      })
      // console.log(reportProblem)
      const myURL = encodeURI(process.env.API_SOM + '&Email=TDCP&Subject=' + subject + '&Detail=' + detail + '&Product=SW00001')
      let { data: ticket } = await axios.get(myURL)
      res.send(ticket)
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.get('/update_data', async (req, res, next) => {
  try {
    var updateDate = await Invoices_ERP.findOne({
      attributes: ['createdAt'],
      order: [['createdAt', 'DESC']]
    })
    var str = updateDate.createdAt.toISOString()
    var year = str.substr(0, 4)
    var month = str.substr(5, 2)
    var day = str.substr(8, 2)
    var createdAt = day + '/' + month + '/' + year
    res.send(createdAt)
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

module.exports = router
