/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
const express = require('express')
const router = express.Router()
const axios = require('axios')
const { Bank, Company, Transaction, Payment_Tracking } = require('../models/index')
const sequelize = require('sequelize')
const Op = sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const CryptoJS = require('crypto-js')
const jwt = require('jsonwebtoken')
var authen = require('./authen')
var multer = require('multer')
var readXlsxFile = require('read-excel-file/node')
var upload_excel = multer({ dest: 'uploads/excel/' })
var sha256 = require('js-sha256')
var moment = require('moment')
const gen = require('./gen')
const fs = require('fs')


async function check_format_invoice (data) {
  console.log('check_format_invoice')
  console.log(data)
  var no_format = /^[A-Za-z0-9-]*$/
  var number_format = /^[0-9]*$/
  var name_eng = /^[A-Z]*$/
  var name_eng_format = /^[A-Za-z0-9(). ]*$/
  var name_account = /^[a-zA-Z\s]+$/
  // var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  var mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  var status_upload = 'format success'
  // console.log('data.length: '+String(data.length))

  for (let i = 0; i < data.length; i++) {
    // console.log('income_type : '+data[i].income_type)
    let doc_date = data[i].doc_date
    let receiver_tax_id = data[i].receiver_tax_id
    let receiver_account_name = data[i].receiver_account_name
    let receiver_account_no = data[i].receiver_account_no
    let receiver_bank_code = data[i].receiver_bank_code.toUpperCase()
    let receiver_mobile = data[i].receiver_mobile
    let receiver_email = data[i].receiver_email
    let receiver_type = data[i].receiver_type
    let income_type =data[i].income_type
    let amount_exc_vat = data[i].amount_exc_vat
    let vat_amount = data[i].vat_amount
    let wht_rate = data[i].wht_rate
    let wht_amount = data[i].wht_amount
    let wht_condition = data[i].wht_condition.toUpperCase()

    var bank = await Bank.findOne({ where: { name: receiver_bank_code } })

    if (!doc_date || moment(doc_date, 'YYYYMMDD', true).isValid() === false) {
      status_upload = 'format error'
      data[i].action_description = 'Doc Date format must be YYYYMMDD'
    } 
    else if (!receiver_account_no || !number_format.test(receiver_account_no) ) {
      status_upload = 'format error'
      data[i].action_description = 'Receiver Account No must be number'
    } 
    else if (!receiver_account_name || !name_account.test(receiver_account_name)) {
      status_upload = 'format error'
      data[i].action_description = 'Receiver Company No must be character'
    } 
    else if (!receiver_bank_code || !name_eng.test(receiver_bank_code)) {
      status_upload = 'format error'
      data[i].action_description = 'Receiver Bank Code No must be character'
    } 
    else if (receiver_bank_code && !bank) {
      status_upload = 'format error'
      data[i].action_description = 'Receiver Bank code not found in the system'
    } 
    else if (!receiver_tax_id || !number_format.test(receiver_tax_id) || receiver_tax_id.length != 13 ) {
        status_upload = 'format error'
        data[i].action_description = 'Receiver Tax ID must be 13 digit number'
    }   
    else if (receiver_mobile && (!number_format.test(receiver_mobile) || receiver_mobile.length != 10) ) {
      status_upload = 'format error'
      data[i].action_description = 'Receiver Mobile must be 10 digit number'
    }   
    else if ( (receiver_email && !mailformat.test(receiver_email)) || !receiver_email) {
        status_upload = 'format error'
        data[i].action_description = 'Receiver Email must be format email'
    } 
    else if (!receiver_type || (receiver_type && (receiver_type != 'company' && receiver_type != 'personal') ) ) {
      status_upload = 'format error'
      data[i].action_description = 'Receiver Type must be personal, company'
    }     
    else if (!income_type || !number_format.test(income_type) || income_type.length != 3) {
        status_upload = 'format error'
        data[i].action_description = 'Income Type must be 3 digit number'
    }
    else {
      if (wht_amount != '0') {
        if (!wht_condition || (wht_condition && (wht_condition != 'A'&& wht_condition != 'O' && wht_condition != 'R') ) ) {
          status_upload = 'format error'
          data[i].action_description = 'Wht Condition must be A, O, R'
        } 
      }
      if (wht_amount == '0' && wht_condition) {
        status_upload = 'format error'
        data[i].action_description = 'Wht Condition must not have'
      }
      if (wht_amount.toString().split('.')[1]) {
        if (wht_amount.toString().split('.')[1].length > 2) {
          status_upload = 'format error'
          data[i].action_description = 'Wht Amount must not more than 2 decimal'
        }
      } 
      if (wht_rate.toString().split('.')[1]) {
        if (wht_rate.toString().split('.')[1].length > 2) {
          status_upload = 'format error'
          data[i].action_description = 'Wht Rate must not more than 2 decimal'
        }
      }   
      if (wht_amount != '0' && wht_rate == '0') {
          status_upload = 'format error'
          data[i].action_description = 'Wht Rate must have' 
      } 
      if (wht_amount == '0' && wht_rate != '0') {
        status_upload = 'format error'
        data[i].action_description = 'Wht Rate must not have' 
      }  
      if (vat_amount.toString().split('.')[1]) {
        if (vat_amount.toString().split('.')[1].length > 2) {
          status_upload = 'format error'
          data[i].action_description = 'Vat Amount must not more than 2 decimal'
        }
      } 
      if (amount_exc_vat.toString().split('.')[1]) {
        if (amount_exc_vat.toString().split('.')[1].length > 2) {
          status_upload = 'format error'
          data[i].action_description = 'Amount Exclude Vat must not more than 2 decimal'
        }
      } 
    }
  } // for
  return ({ data, 'status': status_upload })
}


async function check_duplicate_invoice (data) {
  var status_upload = 'no duplicate Invoice No'
  for (let i = 0; i < data.length; i++) {
    var payment_tracking = await db.sequelize.query(
      "select tp.order_id, p.rcode "+
      "from Transaction_Payments tp "+
      "left join Payment_Trackings p "+
      "on p.id = tp.tracking_id "+
      "where tp.order_id = :order_id and p.rcode = '0' ",
      {
        replacements: { order_id:  data[i].order_id },
        type: QueryTypes.SELECT
      }
    )
    if (payment_tracking.length > 0) {
      status_upload = 'duplicate Invoice No'
      data[i].action_description = 'This Doc No. has been processed'
    } else {
      let transaction = await Transaction.findOne({ where: { order_id: data[i].order_id } })
      if (transaction) {
        status_upload = 'duplicate Invoice No'
        data[i].action_description = 'Duplicate Doc No.'
      }
    }
  }
  return ({ data, 'status': status_upload })
}


async function add_invoice (data) {
  for (let i = 0; i < data.length; i++) {
    var bank = await Bank.findOne({ where: { name: data[i].receiver_bank_code, } })
    if (data[i].action_description === 'overwrite') {
      await Transaction.update({ 
        order_id: data[i].order_id,
        doc_date: data[i].doc_date,
        detail: data[i].detail,
        payer_tax_id: data[i].payer_tax_id,
        receiver_tax_id: data[i].receiver_tax_id,
        receiver_company: data[i].receiver_company,
        receiver_account_no: data[i].receiver_account_no,
        receiver_account_name: data[i].receiver_account_name,
        receiver_bank_code: bank.code,
        receiver_type: data[i].receiver_type,
        receiver_mobile: data[i].receiver_mobile,
        receiver_email: data[i].receiver_email,
        income_type: data[i].income_type,
        amount_exc_vat: data[i].amount_exc_vat,
        vat_amount: data[i].vat_amount,
        amount_inc_vat: String(parseFloat(data[i].amount_exc_vat)+parseFloat(data[i].vat_amount)),
        wht_rate: data[i].wht_rate,
        wht_amount: data[i].wht_amount,
        wht_condition: data[i].wht_condition,
        createdBy: data[i].createdBy,
        updatedAt: new Date()
      },
      { where: { order_id: data[i].order_id } })
    } else {
      await Transaction.create({
        order_id: data[i].order_id,
        doc_date: data[i].doc_date,
        detail: data[i].detail,
        payer_tax_id: data[i].payer_tax_id,
        receiver_company: data[i].receiver_company,
        receiver_tax_id: data[i].receiver_tax_id,
        receiver_account_no: data[i].receiver_account_no,
        receiver_account_name: data[i].receiver_account_name,
        receiver_bank_code: bank.code,
        receiver_type: data[i].receiver_type,
        receiver_mobile: data[i].receiver_mobile,
        receiver_email: data[i].receiver_email,
        income_type: data[i].income_type,
        amount_exc_vat: data[i].amount_exc_vat,
        vat_amount: data[i].vat_amount,
        amount_inc_vat: String(parseFloat(data[i].amount_exc_vat)+parseFloat(data[i].vat_amount)),
        wht_rate: data[i].wht_rate,
        wht_amount: data[i].wht_amount,
        wht_condition: data[i].wht_condition,
        createdBy: data[i].createdBy
      })
    }
  }
  return ({ 'status': 'success' })
}


router.post('/check_file_invoice', async (req, res, next) => {
  try {
    // res.send(req.body.data)
    var token = req.headers.authorization
    var validate_token = authen.validate_token(token)
    var hash_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      var invoice_data = []
      for (let i = 0; i < req.body.data.length; i++) {
        if (req.body.data[i].action_description != 'skip') {
          let invoice_view = {}
          Object.assign(invoice_view, {
            'no': i + 1,
            'order_id': req.body.data[i].order_id,
            'doc_date': req.body.data[i].doc_date,
            'detail': req.body.data[i].detail,
            'payer_tax_id': req.body.data[i].payer_tax_id,
            'receiver_company': req.body.data[i].receiver_company,
            'receiver_tax_id': req.body.data[i].receiver_tax_id,
            'receiver_account_no': req.body.data[i].receiver_account_no,
            'receiver_account_name': req.body.data[i].receiver_account_name,
            'receiver_bank_code': req.body.data[i].receiver_bank_code,
            'receiver_type': req.body.data[i].receiver_type,
            'receiver_mobile': req.body.data[i].receiver_mobile,
            'receiver_email': req.body.data[i].receiver_email,
            'income_type': req.body.data[i].income_type,
            'amount_exc_vat': req.body.data[i].amount_exc_vat,
            'vat_amount': req.body.data[i].vat_amount,
            'wht_rate': req.body.data[i].wht_rate,
            'wht_amount': req.body.data[i].wht_amount,
            'wht_condition': req.body.data[i].wht_condition,
            'createdBy': req.body.data[i].createdBy,
            'action_description': req.body.data[i].action_description
          })
          invoice_data.push(invoice_view)
        }
      }
      if (req.body.status === 'no duplicate Invoice No in excel file') {
        var format_company = await check_format_invoice(invoice_data)
        // res.send({ status: format_company.status })
        if (format_company.status === 'format error') {
          res.send(format_company)
        }
        // res.send({ status: format_company.status })
        var duplicate_company = await check_duplicate_invoice(invoice_data)
        if (duplicate_company.status === 'duplicate Invoice No') {
          res.send(duplicate_company)
        } else {
          res.send({ status: 'success' })
        }
      } else if (req.body.status === 'confirm') {
        await add_invoice(invoice_data)
        res.send({ status: 'success' })
      }
    } // valid token
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/upload_file_invoice', upload_excel.single('file'), async (req, res, next) => {
  try {
    var token = req.headers.authorization
    var createdBy = req.body.createdBy
    var validate_token = authen.validate_token(token)
    if (validate_token.status == 'invalid token') {
      res.send(401)
    } else {
      var payer_tax_id = validate_token.source.sub
      // var company = await Company.findOne({ where: { sender_tax_id: sender_tax_id } })
      // fs.renameSync('uploads/excel/' + req.file.filename, 'uploads/excel/' + req.file.originalname)
      // var filename = 'uploads/excel/' + req.file.originalname
      var filename = 'uploads/excel/' + req.file.filename
      var check_sheetname = 'no have'
      var sheets = await readXlsxFile(filename, { getSheets: true })
      var first_sheetname = sheets[0].name
      var rows = await readXlsxFile(filename, { sheet: first_sheetname })
      // res.send(rows)
      if (rows[0].length == 17) {
        let invoice_data = []
        let status_upload = 'no duplicate PV No in excel file'
        for (let i in rows) {
          if (i>0) {
            let ck_income = String(rows[i][11])

            // if (typeof rows[i][9] == "number") console.log('number')
            // else if (typeof rows[i][9] == "string") console.log('string')
            if (ck_income.length == 1) ck_income = '00'+ck_income
            else if (ck_income.length == 2) ck_income = '0'+ck_income

            var vat_amount = String(rows[i][13])
            var vat_split = vat_amount.split('.')
            //case ผูกสูตร
            if (vat_split[1] && vat_split[1].substring(0, 4) == '0000') { vat_amount = vat_split[0] }

            var wht_amount = String(rows[i][15])
            var wht_split = wht_amount.split('.')
            //case ผูกสูตร
            if (wht_split[1] && wht_split[1].substring(0, 4) == '0000') { wht_amount = wht_split[0] }

            // console.log('vat_amount: '+rows[i][10])
            let invoice_view = {}
            Object.assign(invoice_view, {
              'no': i,
              'order_id': rows[i][0] ? String(rows[i][0]) : '',
              'doc_date': rows[i][1] ? String(rows[i][1]) : '',
              'detail': rows[i][2] ? String(rows[i][2]) : '',
              'payer_tax_id': payer_tax_id,
              'receiver_company': rows[i][3] ? String(rows[i][3]) : '',
              'receiver_tax_id': rows[i][4] ? String(rows[i][4]) : '',
              'receiver_account_name': rows[i][5] ? String(rows[i][5]) : '',
              'receiver_account_no': rows[i][6] ? String(rows[i][6]) : '',
              'receiver_bank_code': rows[i][7] ? String(rows[i][7]) : '',
              'receiver_type': rows[i][8] ? String(rows[i][8]) : '',
              'receiver_mobile': rows[i][9] ? String(rows[i][9]) : '',
              'receiver_email': rows[i][10] ? String(rows[i][10]) : '',
              'income_type': ck_income ? ck_income : '',
              'amount_exc_vat': rows[i][12] ? String(rows[i][12]) : '0',
              'vat_amount': rows[i][13] ? vat_amount : '0',
              'wht_rate': rows[i][14] ? String(rows[i][14]) : '0',
              'wht_amount': rows[i][15] ? wht_amount : '0',
              'wht_condition': rows[i][16] ? String(rows[i][16]) : '',
              'createdBy': createdBy,
              'action_description': ''
            })
            invoice_data.push(invoice_view)
          }
        }
        // 'income_type': rows[i][8] ? String(rows[i][8]) : '',

        // fs.unlinkSync('uploads/excel/' + req.file.originalname) // remove file
        fs.unlinkSync('uploads/excel/' + req.file.filename) // remove file
        for (let i = 0; i < invoice_data.length; i++) {
          for (let j = i + 1; j < invoice_data.length; j++) {
            if (invoice_data[i].order_id === invoice_data[j].order_id) {
              status_upload = 'duplicate Invoice No in excel file'
              invoice_data[j].action_description = 'Duplicate Doc No. in excel file'
            }
          }
        }
        if (status_upload === 'duplicate Invoice No in excel file') {
          res.send({ 'data': invoice_data, 'status': status_upload }) 
        }

        let format_invoice = await check_format_invoice(invoice_data)
        if (format_invoice.status === 'format error') res.send(format_invoice)

        let duplicate_invoice = await check_duplicate_invoice(invoice_data)
        if (duplicate_invoice.status === 'duplicate Invoice No') res.send(duplicate_invoice)
        res.send({ 'data': invoice_data, 'status': 'wait confirm' })

      } else {
        res.send({ 'status': 'Incomplete format' })
      }
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router

