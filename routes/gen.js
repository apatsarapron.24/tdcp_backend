const fs = require('fs')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')

module.exports = {
  log_name: async function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();
    if(dd<10) { dd='0'+dd } 
    if(mm<10) { mm='0'+mm } 
    var today_date = String(yyyy)+String(mm)+String(dd)   

    var date_folder = './error_logs/'+today_date;
    if (!fs.existsSync(date_folder)){
        fs.mkdirSync(date_folder);
    }

    var time_hh = today.getHours()
    var time_mm = today.getMinutes()
    var time_ss = today.getSeconds()
    if(time_hh <10) { time_hh='0'+String(time_hh) } 
    if(time_mm <10) { time_mm='0'+String(time_mm) }   
    if(time_ss<10) { time_ss='0'+String(time_ss) } 
    var today_time = String(time_hh)+String(time_mm)+String(time_ss)
    var log_name = "./error_logs/"+today_date+"/"+today_date+'_'+today_time+".txt"
    return log_name
  },
  add_comma: async function (number) {
    var str = number.toString().split('.');
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');    
  },
  payment_no: async function (tranfer_type) {
      try {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();

        if(dd<10) { dd='0'+dd } 
        if(mm<10) { mm='0'+mm } 
        var today_date = String(yyyy)+String(mm)+String(dd)   
        var payment_no = tranfer_type+today_date
        // console.log('payment_no')
        // console.log(payment_no)
        var data = await db.sequelize.query(
          'select TOP (1) payment_no ' +
          'from PayOffs ' +
          "where payment_no like '"+ payment_no  + "%'"+
          'order by id desc',
          {
            type: QueryTypes.SELECT
          }
        )   

        if (data.length > 0) {
          var index = String(parseInt(data[0].payment_no.substring(9)) + 1)
          if (tranfer_type == 'RD') {
            index = String(parseInt(data[0].payment_no.substring(10)) + 1)
          }
          while (index.length < 4) {
            index = '0' + index
          }
          payment_no = payment_no + index
        } else {
          payment_no = payment_no + '0001'
        }
        return payment_no
      } catch (error) {
        console.log(error)
        return ({ status: 'Error gen_no' })
      }
   }  
}
