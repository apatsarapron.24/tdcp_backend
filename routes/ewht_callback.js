/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Payment_Tracking, Company, Transaction_Payment, 
        PayOff, WHT, RD, Log_RDdata, Log_PayDC } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const nodemailer = require('nodemailer')
const { rmSync } = require('fs')
const  moment  = require('moment')
const gen = require('./gen')
const fs = require('fs')



router.post('/ewht/callback', async (req, res, next) => { // billpay hook (background)
  try {
    var callback_data = req.body
    var callback_data_result = req.body.rd_result 
    // console.log('/ewht/callback ')
    // console.log(callback_data_result)
    var payoff_data = await PayOff.findOne({ where: { payment_no: callback_data_result[0].transaction_ref } })
    //receiver
    if (payoff_data.tranfer_type == 'Receiver') {
      await Log_RDdata.update({  
        callback_data: JSON.stringify(callback_data)
        },{ where: { tracking_id: payoff_data.tracking_id, data_type: 'validate', callback_data: { [Op.eq]: null } } 
      })

     data_index = 0 
     for(let data_check of callback_data_result){
        if(data_check.process == 'validated' && data_check.status == 'success'){
          data_index = data_index + 1
        }
      }
      //success all
      if(data_index == callback_data_result.length){
        // console.log('success')
        var list_payment_no = []
        for(let data_for_success of callback_data_result){
          list_payment_no.push(data_for_success.transaction_ref)
          await WHT.update({ 
            process: data_for_success.process,
            status: data_for_success.status,
            description: data_for_success.description,
            validate_transdate: data_for_success.transaction_date,
            validate_transtime: data_for_success.transaction_time
            }, { 
            where: { payment_no: data_for_success.transaction_ref }
          })
        }

        var wht_data = await db.sequelize.query(
          "select SUM(w.wht_amount) as total_wht, w.rd_ref, w.payer_tax_id "+
          "from WHTs as w "+
          "where w.payment_no in (:list_payment_no) and w.process = 'validated' and w.status = 'success' "+
          "group by w.rd_ref, w.payer_tax_id ",
          {
            replacements: { list_payment_no: list_payment_no },
            type: QueryTypes.SELECT
          }) 

          await PayOff.update({ 
            status_code: '000',
            status_message: 'In process'
            }, { 
            where: { payment_no: wht_data[0].rd_ref }
        })

        //call api rd:directcredit
        var sender_data = await Company.findOne()
        var rd_data = await PayOff.findOne({ where: { payment_no: wht_data[0].rd_ref } })

        var direct_credit_data = {
          sender_info : {
            id: sender_data.company_id,
            name: sender_data.name,
            tax_id: sender_data.sender_tax_id,
            account_no: sender_data.bank_book_no,
            bank_code: sender_data.bank_code,
            branch_code: sender_data.branch_code
          },
          receiver_info: [{
            transaction_ref: rd_data.payment_no,
            name: rd_data.receiver_account_name,
            tax_id: rd_data.receiver_tax_id,
            account_no: rd_data.receiver_account_no,
            bank_code: rd_data.receiver_bank_code,
            branch_code: rd_data.receiver_branch_code,
            amount: String(rd_data.pay_amount)
          }]
        }
        // console.log('rd_directcredit')
        // console.log(direct_credit_data)
        await receiver_ewht_noti(list_payment_no)
        try {
          var log_RDdata = await Log_PayDC.create({ 
            tracking_id: payoff_data.tracking_id, tranfer_type: 'RD', call_data: JSON.stringify(direct_credit_data) 
          })
          await axios.post(process.env.API_HOST+'/rd_directcredit', direct_credit_data)  
          res.send('Success')      
        } catch (error) {
          console.log(error)
          await log_RDdata.update({ call_data: 'error' })
          // await WHT.destroy({ where: { rd_ref: payoff_rd[0].payment_no } })
          var error_log_name = await gen.log_name()
          const error_console = new console.Console(fs.createWriteStream(error_log_name))
          error_console.log(error)
          res.send({status: 'Error: RD Direct Credit'}) 
        }             

      } else { // no success all
        var list_payment_no = []
        for(let data_for_fail of callback_data_result){
          list_payment_no.push(data_for_fail.transaction_ref)
          await WHT.update({ 
            process: data_for_fail.process,
            status: data_for_fail.status,
            description: data_for_fail.description,
            validate_transdate: data_for_fail.transaction_date,
            validate_transtime: data_for_fail.transaction_time
            }, { 
            where: { payment_no: data_for_fail.transaction_ref }
          })
        }
        await receiver_ewht_noti(list_payment_no)
        res.send('Success')  
      }

    } else { //RD  
      await Log_RDdata.create({ 
        tracking_id: payoff_data.tracking_id, data_type: 'reconcile', 
        call_data: 'do not call', callback_data: JSON.stringify(callback_data)
      })

     var list_payment_no_rd = []
      for(let data_rd of callback_data_result){
        list_payment_no_rd.push(data_rd.transaction_ref)
        await WHT.update({ 
          process: data_rd.process,
          status: data_rd.status,
          description: data_rd.description,
          reconclie_transdate: data_rd.transaction_date,
          reconclie_transtime: data_rd.transaction_time
          }, { 
          where: { rd_ref: data_rd.transaction_ref, process: 'validated', status: 'success' } 
        })
      }

      //noti rd
      var noti_rd_wht = await db.sequelize.query(
        "select pa.name_th,pa.tax_id,w.receiver_tax_id,w.wht_amount,w.wht_condition, "+
        "       w.reconclie_transdate,w.reconclie_transtime,w.process,w.status,  "+
        "       t.vat_amount, t.order_id,w.rd_ref,w.income_type,w.createdBy, "+
        "       t.receiver_email,p.receiver_account_name "+
        "from WHTs as w "+
        "left join PayOffs as p on w.payment_no = p.payment_no  "+
        "left join Payers as pa on p.payer_tax_id = pa.tax_id  "+
        "left join Transactions as t on p.order_id = t.order_id "+
        "where w.rd_ref in (:payment_no) ",
        {
          replacements: { payment_no: list_payment_no_rd },
          type: QueryTypes.SELECT
        })

        for(let text_rd_wht of noti_rd_wht){
          var wht_amount = await gen.add_comma(text_rd_wht.wht_amount)
          var vat_amount = await gen.add_comma(text_rd_wht.vat_amount)
          var status_rd = 'โอนเงินไปปลายทางไม่สำเร็จ กรุณาเข้าทำรายการอีกครั้ง'
          var date_rd_wht = moment(text_rd_wht.reconclie_transdate).format('DD/MM/YYYY')
          var time_rd_wht = text_rd_wht.reconclie_transtime.substring(0,2)+":"+text_rd_wht.reconclie_transtime.substring(2,4)
          if (text_rd_wht.process == 'reconciled' && text_rd_wht.status == 'success'){
            status_rd = 'การนำส่งภาษีสำเร็จ'
          }else if (text_rd_wht.process == 'reconciled' && text_rd_wht.status == 'fail'){
            status_rd = 'การนำส่งภาษีไม่สำเร็จ'
          }
           var acc_oneid = text_rd_wht.createdBy.split(':') 
           var msg = "ระบบได้นำส่งภาษี"+
                     "\nผู้สั่งจ่าย: "+text_rd_wht.name_th+
                     "\nเลขประจำตัวผู้เสียภาษีอากรของผู้จ่าย: "+text_rd_wht.tax_id+
                     "\nเลขประจำตัวผู้เสียภาษีอากรของผู้รับ: "+text_rd_wht.receiver_tax_id+
                     "\nวัน เวลา: "+date_rd_wht+" "+time_rd_wht+
                     "\nจำนวนเงินภาษีหัก ณ ที่จ่าย: "+wht_amount+" บาท"+
                     "\nจำนวนภาษีมูลค่าเพิ่ม: "+vat_amount+" บาท"+
                     "\nหมายเลขรายการนำจ่าย: "+text_rd_wht.order_id+
                     "\nหมายเลขอ้างอิงของกรมสรรพากร: "+text_rd_wht.rd_ref+
                     "\nประเภทของเงินได้พึงประเมิน: "+text_rd_wht.income_type+
                     "\nสถานะ: "+status_rd+
                     "\nตรวจสอบเพิ่มเติมได้ที่"+
                     "\nhttps://rdserver.rd.go.th/publish/index.php"
           
            var api_noti_group = await axios.post(process.env.API_HOST + '/notification/onechat', {
              oneid: process.env.ADMIN_GROUP,
              msg: msg,
            })
            var api_noti_user = await axios.post(process.env.API_HOST + '/notification/onechat', {
              oneid: acc_oneid[0],
              msg: msg,
            })
            var api_noti_mailsender = await axios.post(process.env.API_HOST + '/notification/email/reconciled', {
              to: acc_oneid[1],
              user: text_rd_wht.name_th,
              payer: text_rd_wht.name_th,
              payer_tax_id: text_rd_wht.tax_id,
              date_time: date_rd_wht+" "+time_rd_wht,
              receiver: text_rd_wht.receiver_account_name,
              receiver_tax_id: text_rd_wht.receiver_tax_id,
              vat: vat_amount,
              wht: wht_amount,
              pv_no: text_rd_wht.order_id,
              rd_ref: text_rd_wht.rd_ref,
              wht_condition: text_rd_wht.wht_condition,
              income_type: text_rd_wht.income_type,
              status: status_rd
            })

           var api_noti_mailreceiver = await axios.post(process.env.API_HOST + '/notification/email/reconciled', {
              to: text_rd_wht.receiver_email,
              user: text_rd_wht.name_th,
              payer: text_rd_wht.name_th,
              payer_tax_id: text_rd_wht.tax_id,
              date_time: date_rd_wht+" "+time_rd_wht,
              receiver: text_rd_wht.receiver_account_name,
              receiver_tax_id: text_rd_wht.receiver_tax_id,
              vat: vat_amount,
              wht: wht_amount,
              pv_no: text_rd_wht.order_id,
              rd_ref: text_rd_wht.rd_ref,
              wht_condition: text_rd_wht.wht_condition,
              income_type: text_rd_wht.income_type,
              status: status_rd
         })

        }
    }
    res.send('Success')
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }

})


async function receiver_ewht_noti (list_payment_no) {
  var noti_data_wht = await db.sequelize.query(
    "select pa.name_th, w.validate_transdate, w.validate_transtime, t.amount_exc_vat, "+
    "       t.vat_amount, t.wht_amount, po.order_id, w.rd_ref, po.receiver_account_name, "+
    "       po.receiver_account_no, b.name as bankcode, w.wht_condition, w.income_type, "+
    "       w.process, w.status, w.description, w.createdBy "+
    "from WHTs as w "+
    "left join PayOffs as po on w.payment_no = po.payment_no  "+
    "left join Transactions as t on po.order_id = t.order_id "+
    "left join Payers as pa on w.payer_tax_id = pa.tax_id "+
    "left join Banks as b on po.receiver_bank_code = b.code "+
    "where w.payment_no in (:list_payment_no) ",
    {
      replacements: { list_payment_no: list_payment_no },
      type: QueryTypes.SELECT
  })

  for(let text_noti_wht of noti_data_wht){
    var amount_exc_vat = await gen.add_comma(text_noti_wht.amount_exc_vat)
    var vat_amount = await gen.add_comma(text_noti_wht.vat_amount)
    var wht_amount = await gen.add_comma(text_noti_wht.wht_amount)
    var status_msg = 'การนำส่งข้อมูลรายการภาษีไม่สำเร็จ เนื่องจาก'+text_noti_wht.description
    var date_wht = moment(text_noti_wht.validate_transdate).format('DD/MM/YYYY')
    var time_wht = text_noti_wht.validate_transtime.substring(0,2)+":"+text_noti_wht.validate_transtime.substring(2,4)
    if(text_noti_wht.process == 'validated' && text_noti_wht.status == 'success'){
      status_msg = 'การนำส่งข้อมูลรายการภาษีสำเร็จ'
    }else if(text_noti_wht.process == 'validated' && text_noti_wht.status == 'fail'){
      status_msg = 'การนำส่งข้อมูลรายการภาษีไม่สำเร็จ เนื่องจาก'+text_noti_wht.description
    }
    if(text_noti_wht.process == 'validated'){
      var acc_oneid = text_noti_wht.createdBy.split(':') 
      var msg = "ระบบได้นำส่งข้อมูลรายการภาษี"+
                "\nผู้สั่งจ่าย: "+text_noti_wht.name_th+
                "\nวัน เวลา: "+date_wht+" "+time_wht+
                "\nจำนวนเงินไม่รวมภาษีมูลค่าเพิ่ม: "+amount_exc_vat+" บาท"+
                "\nจำนวนเงินภาษีมูลค่าเพิ่ม: "+vat_amount+" บาท"+
                "\nจำนวนเงินภาษีหัก ณ ที่จ่าย: "+wht_amount+" บาท"+
                "\nหมายเลขรายการนำจ่าย: "+text_noti_wht.order_id+
                "\nหมายเลขอ้างอิงของกรมสรรพากร: "+text_noti_wht.rd_ref+
                "\nชื่อบัญชีของผู้รับเงิน: "+text_noti_wht.receiver_account_name+
                "\nเลขที่บัญชีของผู้รับเงิน: "+text_noti_wht.receiver_account_no+
                "\nธนาคารของผู้รับเงิน: "+text_noti_wht.bankcode+
                "\nเงื่อนไขของภาษีหัก ณ ที่จ่าย: "+text_noti_wht.wht_condition+
                "\nประเภทของรายได้: "+text_noti_wht.income_type+
                "\nสถานะ: "+status_msg+
                "\nตรวจสอบเพิ่มเติมได้ที่"+
                "\nhttps://epayment.inet.co.th"
      
      var api_noti_group = await axios.post(process.env.API_HOST + '/notification/onechat', {
        oneid: process.env.ADMIN_GROUP,
        msg: msg,
      })
      var api_noti_user = await axios.post(process.env.API_HOST + '/notification/onechat', {
        oneid: acc_oneid[0],
        msg: msg,
      })
      var api_noti_mailewht = await axios.post(process.env.API_HOST + '/notification/email/ewht', {
        to: acc_oneid[1],
        user: text_noti_wht.name_th,
        date_time: date_wht+" "+time_wht,
        amount: amount_exc_vat,
        vat: vat_amount,
        wht: wht_amount,
        pv_no: text_noti_wht.order_id,
        rd_ref: text_noti_wht.rd_ref,
        account_name: text_noti_wht.receiver_account_name,
        account_no: text_noti_wht.receiver_account_no,
        bank: text_noti_wht.bankcode,
        wht_condition: text_noti_wht.wht_condition,
        income_type: text_noti_wht.income_type,
        status: status_msg
      })

    } else if (text_noti_wht.process == 'acknowledged'){
      var acc_oneid = text_noti_wht.createdBy.split(':') 
      var msg = "ระบบได้นำส่งข้อมูลรายการภาษี"+
                "\nผู้สั่งจ่าย: "+text_noti_wht.name_th+
                "\nวัน เวลา: "+date_wht+" "+time_wht+
                "\nจำนวนเงินไม่รวมภาษีมูลค่าเพิ่ม: "+amount_exc_vat+" บาท"+
                "\nจำนวนเงินภาษีมูลค่าเพิ่ม: "+vat_amount+" บาท"+
                "\nจำนวนเงินภาษีหัก ณ ที่จ่าย: "+wht_amount+" บาท"+
                "\nหมายเลขรายการนำจ่าย: "+text_noti_wht.order_id+
                "\nหมายเลขอ้างอิงของกรมสรรพากร: "+text_noti_wht.rd_ref+
                "\nชื่อบัญชีของผู้รับเงิน: "+text_noti_wht.receiver_account_name+
                "\nเลขที่บัญชีของผู้รับเงิน: "+text_noti_wht.receiver_account_no+
                "\nธนาคารของผู้รับเงิน: "+text_noti_wht.bankcode+
                "\nเงื่อนไขของภาษีหัก ณ ที่จ่าย: "+text_noti_wht.wht_condition+
                "\nประเภทของรายได้: "+text_noti_wht.income_type+
                "\nสถานะ: "+status_msg+
                "\nตรวจสอบเพิ่มเติมได้ที่"+
                "\nhttps://epayment.inet.co.th"
      
      var api_noti_group = await axios.post(process.env.API_HOST + '/notification/onechat', {
        oneid: process.env.ADMIN_GROUP,
        msg: msg,
      })
    }
  }
  return ({ 'status': 'success' })
}


router.post('/rd_directcredit', async (req, res, next) => {
  try {
    console.log('rd_directcredit')
    console.log(req.body)
    res.send('Success')
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router
