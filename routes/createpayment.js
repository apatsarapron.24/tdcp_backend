/*eslint-disable camelcase */
const express = require('express')
const router = express.Router()
const axios = require('axios')
const generate = require('nanoid/generate')
// eslint-disable-next-line camelcase
const { Transaction, Payment_Tracking, Company, Transaction_Payment } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const encode_decode = require('./encode_decode')
const querystring = require('querystring')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')


router.post('/createpayment', async (req, res, next) => {
  try {
    var token = req.headers.authorization
    var validate_token = authen.validate_token(token)
    var payer_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      var payer = req.body.payer
      var payType = req.body.pay_type
      var bankNo = req.body.bank
      var email = req.body.email
      var pay_amount = req.body.pay_amount
      var tempinvoice = []
      var index = 0
      var descInvoiceno = ''
      for (let invoiceno of req.body.invoiceno) {
        if (index <= 10) {
          if (index == 0) { descInvoiceno = descInvoiceno + invoiceno } 
          else if (index < 10) { descInvoiceno = descInvoiceno + ', ' + invoiceno } 
          else if (index == 10) { descInvoiceno = descInvoiceno + ', ...' }
          index++
        }
      }

      let orderid = generate('1234567890', 20)
      let payment_data = {
        key: process.env.MKEY,
        orderId: String(orderid),
        orderDesc: String(descInvoiceno),
        amount: pay_amount,
        apUrl: process.env.API_HOST+'/callback_url',
        ud1: '',
        ud2: '',
        ud3: '',
        ud4: '',
        ud5: email,
        lang: 'E',
        bankNo: bankNo,
        currCode: '764',
        payType: payType
      }
      // console.log('payment_data')
      // console.log(payment_data)

      var new_tracking = await Payment_Tracking.create({
        payer_tax_id: payer_tax_id,
        call_access_token: JSON.stringify(payment_data),
        payment_ref: orderid,
        // payment_ref: 'PF2',
        description: descInvoiceno,
        payment_method: payType,
        bank_name: bankNo,
        pay_amount: pay_amount,
        payer: payer
      })

      try {
        var api_gettoken = await axios.post(process.env.API_OPS, payment_data)
        // console.log('api_gettoken')
        // console.log(api_gettoken)
        var link = api_gettoken.data.link
        var accessToken = api_gettoken.data.token
        var paymentRefId = api_gettoken.data.ref1
        await new_tracking.update({ ops_payment_ref: paymentRefId, callback_access_token: 'success' }) 
        // await new_tracking.update({ ops_payment_ref: 'P2', callback_access_token: 'success' }) 

        if (payType === 'CR' || payType === 'AC' || payType === 'CO') {

          if (bankNo === 'BBL' && payType === 'CO') {
            await new_tracking.update({ call_page_bank: 'do not call', callback_page_bank: 'do not callback' }) 
            for (let order_id of req.body.invoiceno) {
              await Transaction_Payment.create({ tracking_id: new_tracking.id,order_id: order_id })
            }
            res.send({ ref1: api_gettoken.data.ref1, ref2: api_gettoken.data.ref2, paymentRefId: paymentRefId })
          } 

          else if (bankNo !== 'SCB') {
            try {
              await new_tracking.update({ call_page_bank: 'success' }) 
              // await new_tracking.update({ call_page_bank: 'success', callback_page_bank: 'do not callback' }) 
              let api_formdata = await axios.post(link, querystring.stringify({ access_token: accessToken }))
              await new_tracking.update({ callback_page_bank: 'success' }) 
              // console.log('api_formdata 1')
              // console.log(api_formdata)
              let tagForm = (api_formdata.data.split('</form>'))[0]
              let tagInput = tagForm.split('method="POST">')[1]
              let tagUrl = (((tagForm.split('method="POST">'))[0]).split('action="')[1]).split('"')[0]
              for (let order_id of req.body.invoiceno) {
                await Transaction_Payment.create({ tracking_id: new_tracking.id,order_id: order_id })
              }
              res.send({ tagInput: tagInput, tagUrl: tagUrl, paymentRefId: paymentRefId })
            } catch (error) {
              console.log(error)
              await new_tracking.update({ callback_page_bank: 'error' }) 
              var error_log_name = await gen.log_name()
              const error_console = new console.Console(fs.createWriteStream(error_log_name))
              error_console.log(error)
              res.send({status: 'Error'}) 
            }

          } else if (bankNo === 'SCB') {
            await new_tracking.update({ call_page_bank: 'do not call', callback_page_bank: 'do not callback' }) 
            let tagInput = '<input hidden name="access_token" type="text" value="' + accessToken + '"><input hidden type="submit" value="PayConfirm">'
            let tagUrl = link
            for (let order_id of req.body.invoiceno) {
              await Transaction_Payment.create({ tracking_id: new_tracking.id, order_id: order_id })
            }
            res.send({ tagInput: tagInput, tagUrl: tagUrl, paymentRefId: paymentRefId })            
          }

        } else { // QR
          try {
            await new_tracking.update({ call_page_bank: 'success' }) 
            let api_image = await axios.post(link, {access_token: accessToken})
            await new_tracking.update({ callback_page_bank: 'success' }) 
            for (let order_id of req.body.invoiceno) {
              await Transaction_Payment.create({ tracking_id: new_tracking.id,order_id: order_id })
            }
            res.send({ imageQR: api_image.data.qrcode, paymentRefId: paymentRefId })
          } catch (error) {
              console.log(error)
              await new_tracking.update({ callback_page_bank: 'error' }) 
              var error_log_name = await gen.log_name()
              const error_console = new console.Console(fs.createWriteStream(error_log_name))
              error_console.log(error)
              res.send({status: 'Error'}) 
          }          
        }
        
      } catch (error) {
        console.log(error)
        await new_tracking.update({ callback_access_token: 'error' }) 
        var error_log_name = await gen.log_name()
        const error_console = new console.Console(fs.createWriteStream(error_log_name))
        error_console.log(error)
        res.send({status: 'Error'}) 
      }


    }// else validate
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router
