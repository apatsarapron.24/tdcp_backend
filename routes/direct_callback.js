/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Payment_Tracking, Company, Transaction_Payment, 
        PayOff, WHT, RD, Payer, Log_PayDC, Log_RDdata } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const nodemailer = require('nodemailer')
const { rmSync } = require('fs')
const  moment  = require('moment')
const gen = require('./gen')
const fs = require('fs')


router.post('/directcredit/callback', async (req, res, next) => { // billpay hook (background)
  try {
    var callback_data = req.body
    var callback_data_result = req.body.direct_credit_result 
    // console.log('/directcredit/callback')
    // console.log(callback_data_result)
    var tranfer_type = (callback_data_result[0].transaction_ref).substring(0, 1)
    var payoff_data = await PayOff.findOne({ where: { payment_no: callback_data_result[0].transaction_ref } })

    if (tranfer_type == 'P') {  
      await Log_PayDC.update({  
        callback_data: JSON.stringify(callback_data)
        },{ where: { tracking_id: payoff_data.tracking_id, callback_data: { [Op.eq]: null } } 
      })

      var list_payment_no = []
      for (let data of callback_data_result) {
        list_payment_no.push(data.transaction_ref)
        await PayOff.update({ 
          status_code: data.trasfer_status_code,
          status_message: data.trasfer_status_message,
          transaction_date: data.transaction_date,
          transaction_time: data.transaction_time
          }, { 
          where: { payment_no: data.transaction_ref }
        })
      }

      var payoff_rd = await db.sequelize.query(
        "select TOP (1) rd.payment_no "+
        "from PayOffs as p left join PayOffs rd on p.tracking_id = rd.tracking_id "+
        "where p.payment_no in (:list_payment_no) and rd.tranfer_type = 'RD' ",
        {
          replacements: { list_payment_no: list_payment_no },
          type: QueryTypes.SELECT
      })

      var data_index = 0
      for(let data of callback_data_result) if(data.trasfer_status_code == '1') data_index = data_index + 1 

      if(data_index == callback_data_result.length){
        var ewht_data = await db.sequelize.query(
          "select p.payment_no, p.receiver_account_name, p.receiver_account_no, "+
          "       p.receiver_bank_code, p.receiver_branch_code, p.receiver_tax_id,  t.wht_amount, t.wht_condition, "+
          "       t.income_type, (t.amount_exc_vat+t.vat_amount) as amount, t.vat_amount, "+
          "       p.transaction_date as payment_date, p.transaction_time as payment_time, "+
          "       p.receiver_tax_id, p.payer_tax_id, p.createdBy "+
          "FROM PayOffs as p "+
          "left join Transactions as t on p.order_id = t.order_id  "+
          "where payment_no in (:list_payment_no) and p.tranfer_type = 'Receiver' and "+
          "      p.status_code = '1' and t.wht_amount != '0' ",
          {
            replacements: { list_payment_no: list_payment_no },
            type: QueryTypes.SELECT
        })

        for (let data of ewht_data) {
          await WHT.create({
            rd_ref: payoff_rd[0].payment_no,
            payment_no: data.payment_no,
            payer_tax_id: data.payer_tax_id,
            receiver_tax_id: data.receiver_tax_id,
            wht_amount: data.wht_amount,
            wht_condition: data.wht_condition,
            income_type: data.income_type,
            process: 'In process',
            createdBy: data.createdBy
          })
        }

        //call api ewht
        var payer_data = await Payer.findOne({ where: { tax_id: ewht_data[0].payer_tax_id } })
        var sender_data = await Company.findOne()

        // call receiver:ewht
        var receiver_data = []
        for (var data of ewht_data) {
          var temp_receiver_date = {}
          Object.assign(temp_receiver_date, {
            'name': data.receiver_account_name,
            'tax_id': data.receiver_tax_id,
            'account': data.receiver_account_no,
            'bank_code': data.receiver_bank_code,
            'branch_code': data.receiver_branch_code,
            'amount': String(data.amount),
            'vat_amount': String(data.vat_amount),
            'wht_amount': String(data.wht_amount),
            'wht_condition': data.wht_condition,
            'type_of_income': data.income_type,
            'payment_reference': data.payment_no,
            'payment_date': data.payment_date,
            'payment_time': data.payment_time
          })
           receiver_data.push(temp_receiver_date)
        }
        var wht_result = {
          sender_info : {
            id: sender_data.company_id,
            name: sender_data.name,
            tax_id: sender_data.sender_tax_id,
            account: sender_data.bank_book_no,
            bank_code: sender_data.bank_code,
            branch_code: sender_data.branch_code
          },
           receiver_info: receiver_data   
        }

        await receiver_dc_noti(list_payment_no, payoff_rd[0].payment_no)
        
        try {
          var log_RDdata = await Log_RDdata.create({ 
            tracking_id: payoff_data.tracking_id, data_type: 'validate', call_data: JSON.stringify(wht_result) 
          })
          await axios.post(process.env.API_HOST+'/receiver_ewht', wht_result)
          res.send('Success')
        } catch (error) {
          console.log(error)
          await log_RDdata.update({ call_data: 'error' })
          // await WHT.destroy({ where: { rd_ref: payoff_rd[0].payment_no } })
          var error_log_name = await gen.log_name()
          const error_console = new console.Console(fs.createWriteStream(error_log_name))
          error_console.log(error)
          res.send({status: 'Error: Receiver Ewht'}) 
        }  

      } 
    } else { //RD
      await Log_PayDC.update({  
        callback_data: JSON.stringify(callback_data)
        },{ where: { tracking_id: payoff_data.tracking_id, tranfer_type: 'RD', callback_data: { [Op.eq]: null } } 
      })

      var list_payment_no_rd = []
      for (let data_rd of callback_data_result) {
        list_payment_no_rd.push(data_rd.transaction_ref)
        await PayOff.update({ 
          status_code: data_rd.trasfer_status_code,
          status_message: data_rd.trasfer_status_message,
          transaction_date: data_rd.transaction_date,
          transaction_time: data_rd.transaction_time
          }, { 
          where: { payment_no: data_rd.transaction_ref } 
        })
      }

      //noti rd:directcredit
      var noti_data_rd_direct = await db.sequelize.query(
        "select pa.name_th, p.* from PayOffs as p "+
        "left join Payers as pa on p.payer_tax_id = pa.tax_id "+
        "where p.payment_no in (:payment_no) ",
        {
        replacements: { payment_no: list_payment_no_rd },
        type: QueryTypes.SELECT
      })

      for(let text_noti_rd of noti_data_rd_direct){
        var createdBy = text_noti_rd.createdBy.split(':') 
        var pay_amount = await gen.add_comma(text_noti_rd.pay_amount)
        var status_msg_rd = 'โอนเงินไปปลายทางไม่สำเร็จ กรุณาเข้าทำรายการอีกครั้ง'
        var date_rd_direct = moment(text_noti_rd.transaction_date).format('DD/MM/YYYY')
        var time_rd_direct = text_noti_rd.transaction_time.substring(0,2)+":"+text_noti_rd.transaction_time.substring(2,4)
        if ( text_noti_rd.status_message == 'Success') {
          status_msg_rd = 'โอนเงินไปปลายทางสำเร็จแล้ว'
        }

        var msg_rd = "ระบบได้โอนเงินไปยัง กรมสรรพากร"+
                 "\nผู้สั่งจ่าย: "+text_noti_rd.name_th+
                 "\nวัน เวลา: "+date_rd_direct+" "+time_rd_direct+
                 "\nจำนวนเงิน: "+pay_amount+
                 "\nหมายเลขรายการนำจ่าย: "+text_noti_rd.order_id+
                 "\nหมายเลขอ้างอิงของกรมสรรพากร: "+text_noti_rd.payment_no+
                 "\nชื่อบัญชี: "+text_noti_rd.receiver_account_name+
                 "\nเลขที่บัญชี: "+text_noti_rd.receiver_account_no+
                 "\nธนาคาร: "+text_noti_rd.receiver_bank_code+
                 "\nสถานะ: "+status_msg_rd+
                 "\nตรวจสอบเพิ่มเติมได้ที่ \nhttps://epayment.inet.co.th"
        await axios.post(process.env.API_HOST + '/notification/onechat', {
          oneid: process.env.ADMIN_GROUP,
          msg: msg_rd,
        })
        await axios.post(process.env.API_HOST + '/notification/onechat', {
          oneid: createdBy[0],
          msg: msg_rd,
        })
        await axios.post(process.env.API_HOST + '/notification/email/directcredit/rd', {
          payer_email:  createdBy[1],
          payer_name: text_noti_rd.name_th,
          date_time: date_rd_direct+" "+time_rd_direct,
          amount: pay_amount,
          pv_no: text_noti_rd.order_id,
          rd_ref: text_noti_rd.payment_no,
          account_name: text_noti_rd.receiver_account_name,
          account_no: text_noti_rd.receiver_account_no,
          bank: text_noti_rd.receiver_bank_code,
          status: status_msg_rd
        })
        
      }
    }
    res.send('Success')
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }

})


async function receiver_dc_noti (list_payment_no, rd_ref) {
  var noti_data_direct = await db.sequelize.query(
    "select pa.name_th, b.name as receiver_name,p.receiver_account_name,t.wht_amount, "+
    "       p.receiver_account_no, p.order_id,p.transaction_date, p.transaction_time, p.status_message, "+
    "       p.createdBy, p.pay_amount "+
    "from PayOffs as p "+
    "left join Transactions as t on p.order_id = t.order_id  "+
    "left join Payers as pa on t.payer_tax_id = pa.tax_id  "+
    "left join Banks as b on t.receiver_bank_code = b.code "+
    "where payment_no in (:list_payment_no)",
    {
      replacements: { list_payment_no: list_payment_no },
      type: QueryTypes.SELECT
  })

  for(let text_noti of noti_data_direct){
    // let rd_ref = ''
    var pay_amount = await gen.add_comma(text_noti.pay_amount)
    let status_msg = 'โอนเงินไปปลายทางไม่สำเร็จ กรุณาเข้าทำรายการอีกครั้ง'
    let date_direct = moment(text_noti.transaction_date).format('DD/MM/YYYY')
    let time_direct = text_noti.transaction_time.substring(0,2)+":"+text_noti.transaction_time.substring(2,4)
    if(text_noti.wht_amount != 0 && text_noti.status_message == 'Success'){
      rd_ref = rd_ref
      status_msg = 'โอนเงินไปปลายทางสำเร็จแล้ว อยู่ระหว่างนำส่งภาษีไปยังสรรพากร'
    }else if(text_noti.wht_amount == 0 && text_noti.status_message == 'Success'){
      status_msg = 'โอนเงินไปปลายทางสำเร็จแล้ว'
    }
    var acc_oneid = text_noti.createdBy.split(':') 
    var msg = "ระบบได้โอนเงินไปยัง "+text_noti.receiver_account_name+
               "\nผู้สั่งจ่าย: "+text_noti.name_th+
               "\nวัน เวลา: "+date_direct+" "+time_direct+
               "\nจำนวนเงิน: "+pay_amount+
               "\nหมายเลขรายการนำจ่าย: "+text_noti.order_id+
               "\nชื่อบัญชี: "+text_noti.receiver_account_name+
               "\nเลขที่บัญชี: "+text_noti.receiver_account_no+
               "\nธนาคาร: "+text_noti.receiver_name+
               "\nสถานะ: "+status_msg+
               "\nตรวจสอบเพิ่มเติมได้ที่ \nhttps://epayment.inet.co.th"
    await axios.post(process.env.API_HOST + '/notification/onechat', { 
      oneid: process.env.ADMIN_GROUP,msg: msg,
    })
    await axios.post(process.env.API_HOST + '/notification/onechat', {
       oneid: acc_oneid[0],
       msg: msg,
    })
    await axios.post(process.env.API_HOST + '/notification/email/directcredit', {
       to: acc_oneid[1],
       user: text_noti.name_th,
       receiver: text_noti.receiver_account_name,
       date_time: date_direct+" "+time_direct,
       amount: pay_amount,
       pv_no: text_noti.order_id,
       rd_ref: rd_ref,
       account_name: text_noti.receiver_account_name,
       account_no: text_noti.receiver_account_no,
       bank: text_noti.receiver_name,
       status: status_msg
    })
  }
  return ({ 'status': 'success' })
}


router.post('/receiver_ewht', async (req, res, next) => {
  try {
    console.log('receiver_ewht_api')
    console.log(req.body)
    res.send('Success')
    // res.status(404).send()
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


// router.get('/aaa', async (req, res, next) => {
//   try {
//     try {
//       console.log('a1')
//       await axios.post(process.env.API_HOST+'/receiver_ewht', {})
//     } catch (error) {
//       console.log('a2')
//     }
//     console.log('a3')
//     res.send('Success')
//     // res.status(404).send()
//   } catch (error) {
//     console.log(error)
//     var error_log_name = await gen.log_name()
//     const error_console = new console.Console(fs.createWriteStream(error_log_name))
//     error_console.log(error)
//     res.send({status: 'Error'}) 
//   }
// })


module.exports = router
