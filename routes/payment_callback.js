/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Payment_Tracking, Company, Payer, 
        Transaction_Payment, PayOff, WHT, RD, Log_PayDC } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const nodemailer = require('nodemailer')
const { rmSync } = require('fs')
const gen = require('./gen')
const fs = require('fs')

String.prototype.splice = function (idx, rem, str) {
  return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem))
}


router.post('/callback', async (req, res, next) => {
  try {
    var callback_data = req.body.detail
    // console.log('/callback')
    // console.log(callback_data)
    var trackingData = await Payment_Tracking.findOne({ 
        where: { ops_payment_ref: callback_data.payment_reference_id, rcode: { [Op.is]: null } } 
    })

    if (trackingData) {
      await trackingData.update({
        rcode: callback_data.response_code,
        payment_status: callback_data.response_message,
        merchant_id: callback_data.merchant_id,
        payment_ref: callback_data.order_id,
        pay_amount: callback_data.receive_amount,
        // payment_method: callback_data.payment_type,
        bank_name: callback_data.payment_acquirer_bank,
        payment_date: callback_data.transaction_date,
        payment_time: callback_data.transaction_time,
        description: callback_data.order_description
      })

      if (callback_data.response_code == '0') { //Success
        var trans_data = await db.sequelize.query(
          "select (t.amount_exc_vat + t.vat_amount - t.wht_amount) as pay_amount, "+
          "       p.id as tracking_id ,p.payer_tax_id as payer_tax_id, p.payer, t.* "+
          "from Transactions as t "+
          "inner join Transaction_Payments as pa on t.order_id = pa.order_id "+
          "inner join Payment_Trackings as p on pa.tracking_id = p.id "+
          "where p.ops_payment_ref = :payment_ref ",
          {
            replacements: { payment_ref:  callback_data.payment_reference_id},
            type: QueryTypes.SELECT
          }
        )

        let wht_index = 0
        let order_id = ''
        var tracking_id = trans_data[0].tracking_id
        var total_amount = 0
        var total_vat = 0
        var total_wht = 0
        var total_pay_amount = 0
        var receiver_data = []
        var payer_tax_id = trans_data[0].payer_tax_id

        for (let data of trans_data) {
          total_amount = total_amount + data.amount_exc_vat
          total_vat = total_vat + data.vat_amount
          total_wht = total_wht + data.wht_amount
          total_pay_amount = total_pay_amount + (data.amount_exc_vat+data.vat_amount-data.wht_amount)
          if (parseInt(data.wht_amount) != 0 && wht_index <= 10) {
            if (wht_index == 0) { order_id = order_id + data.order_id } 
            else if (wht_index < 10) { order_id = order_id + ', ' + data.order_id } 
            else if (wht_index == 10) { order_id = order_id + ', ...' } 
            wht_index++
          }

          // let gen_p_no = await gen_no('P')
          let gen_p_no = await gen.payment_no('P')
          await PayOff.create({
            payment_no: gen_p_no,
            payer_tax_id: data.payer_tax_id,
            order_id: data.order_id,
            tracking_id: tracking_id,
            sender_id: data.company_id,
            receiver_tax_id: data.receiver_tax_id,
            receiver_account_name: data.receiver_account_name,
            receiver_account_no: data.receiver_account_no,
            receiver_bank_code: data.receiver_bank_code,
            receiver_branch_code: '0'+data.receiver_account_no.substring(0, 3),
            tranfer_type: 'Receiver',
            pay_amount: data.pay_amount,
            status_code: '000',
            status_message: 'In process',
            createdBy: data.payer
          })

          //call api directcredit
          var data_receiver = {}
          Object.assign(data_receiver, {
            'transaction_ref': gen_p_no,
            'name': data.receiver_account_name,
            'tax_id': data.receiver_tax_id,
            'account_no': data.receiver_account_no,
            'bank_code': data.bank_code,
            'branch_code': '0'+data.receiver_account_no.slice(0, 3),
            'amount': String(data.pay_amount)
          })
          receiver_data.push(data_receiver)

        }//for

        var sender_data = await Company.findOne()
        var direct_credit_data = {
          sender_info : {
            id: sender_data.company_id,
            name: sender_data.name,
            tax_id: sender_data.sender_tax_id,
            account_no: sender_data.bank_book_no,
            bank_code: sender_data.bank_code,
            branch_code: sender_data.branch_code
           },
          receiver_info: receiver_data   
        }

        var createdBy = trans_data[0].createdBy.split(":")
        var payer_oneid = createdBy[0]
        var payer_email = createdBy[1]
        // console.log('payer_email: '+payer_email)
        var payer_data = await Payer.findOne({ where: { tax_id: payer_tax_id } })

        var rd_ref = ''
        if (wht_index > 0) {
          let rd_data = await RD.findOne({ order: [['id', 'DESC']] })
          // let gen_rd_no = await gen_no('RD')
          // rd_ref = gen_rd_no.payment_no
          rd_ref = await gen.payment_no('RD')
          await PayOff.create({
            payment_no: rd_ref,
            payer_tax_id: payer_tax_id,
            order_id: order_id,
            tracking_id: tracking_id, 
            receiver_tax_id: rd_data.tax_id,
            receiver_account_no: rd_data.account_no,
            receiver_account_name: rd_data.account_name,
            receiver_bank_code: rd_data.bank_code,
            receiver_branch_code: rd_data.branch_code,
            pay_amount: total_wht,
            tranfer_type: 'RD',
            createdBy: trans_data[0].createdBy
          })
        }

        total_list = await gen.add_comma(trans_data.length)
        total_amount = await gen.add_comma(total_amount)
        total_wht = await gen.add_comma(total_wht)
        total_vat = await gen.add_comma(total_vat)
        total_pay_amount = await gen.add_comma(total_pay_amount)

        var msg = "เรียน "+payer_data.name_th+
                   "\n\nขอบคุณสำหรับการใช้งานบริการการชำระเงินออนไลน์ผ่านทาง INET e-Payment"+
                   "\n\nผู้ให้บริการได้รับเงินสำหรับบริการชำระเงิน พร้อมภาษีมูลค่าเพิ่ม และภาษีหัก ณ ที่จ่าย (ถ้ามี) โดยมีรายละเอียดดังนี้"+
                   "\n\nชื่อผู้จ่าย : "+payer_data.name_th+
                   "\nเลขประจำตัวผู้เสียภาษีอากรของผู้จ่าย : "+payer_tax_id+
                   "\nจำนวนรายการ : "+total_list+
                   "\nจำนวนเงินที่ชำระไปยังผู้รับเงิน : "+total_amount+
                   "\nจำนวนเงินภาษีหัก ณ ที่จ่าย : "+total_wht+
                   "\nจำนวนภาษีมูลค่าเพิ่ม :"+total_vat+
                   "\nจำนวนเงินรวม : "+total_pay_amount+
                   "\nหมายเลขอ้างอิง : "+rd_ref+
                   "\n\nหากท่านต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อ Call center 02-xxx-xxxx "+
                   "\n\nขอแสดงความนับถือ"+
                   "บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน)"

        await axios.post(process.env.API_HOST + '/notification/onechat', { oneid: payer_oneid, msg: msg })
        await axios.post(process.env.API_HOST + '/notification/onechat', { oneid: process.env.ADMIN_GROUP, msg: msg })
        await axios.post(process.env.API_HOST + '/notification/email/payment/payer/summary', {
          payer_email: payer_email,
          payer_name: payer_data.name_th,
          payer_taxid: payer_tax_id,
          items: total_list,
          amount: total_amount,
          wht_amount: total_wht,
          vat_amount: total_vat,
          pay_amount: total_pay_amount,
          rd_ref: rd_ref
        })

        for (let data of trans_data) {
          let amount = await gen.add_comma(data.amount_exc_vat)
          let wht_amount = await gen.add_comma(data.wht_amount)
          let vat_amount = await gen.add_comma(data.vat_amount)

          if (parseInt(data.wht_amount) != 0 ) {
            await axios.post(process.env.API_HOST + '/notification/email/payment/payer', {
              payer_name: payer_data.name_th,
              payer_taxid: payer_tax_id,
              payer_email: payer_email,
              receiver_taxid: data.receiver_tax_id,
              income_type: data.income_type,
              amount: amount,
              wht_amount: wht_amount,
              vat_amount: vat_amount,
              rd_ref: rd_ref
            })   

            await axios.post(process.env.API_HOST + '/notification/email/payment/receiver', {
              payer_name: payer_data.name_th,
              payer_taxid: data.payer_tax_id,
              receiver_name: data.receiver_account_name,
              receiver_taxid: data.receiver_tax_id,
              receiver_email: data.receiver_email,
              income_type: data.income_type,
              amount: amount,
              wht_amount: wht_amount,
              vat_amount: vat_amount,
              rd_ref: rd_ref
            })  

          }
        }

        try {
          var log_payDC = await Log_PayDC.create({ tracking_id: tracking_id, tranfer_type: 'Receiver', call_data: JSON.stringify(direct_credit_data) })
          var api_gettoken = await axios.post(process.env.API_HOST+'/receiver_directcredit', direct_credit_data)
          res.send('Success')
        } catch (error) {
          console.log(error)
          await log_payDC.update({ call_data: 'error' })
          await PayOff.destroy({ where: { tracking_id: tracking_id, tranfer_type: 'Receiver' } })
          var error_log_name = await gen.log_name()
          const error_console = new console.Console(fs.createWriteStream(error_log_name))
          error_console.log(error)
          res.send({status: 'Error: Receiver Direct Credit'}) 
        }    

      }//pay success  

    }//rcode is null
    else {
      res.send('duplicate callback')
    }

  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/receiver_directcredit', async (req, res, next) => {
  try {
    console.log('receiver_directcredit_api')
    console.log(req.body)
    res.send('Success')
    // res.status(404).send()
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/callback_url', async (req, res, next) => { //callback_url (foreground)
  try {
    console.log('/callback_url')
    console.log(req.body)
    if (req.body.payType !== 'QR Code Payment') {
      var pay_no = ''
      if (req.body.orderIdRef) {
        pay_no = req.body.orderIdRef
      } else {
        pay_no = req.body.orderIDRef
      }
      let token = authen.create_token(pay_no)
      res.redirect(process.env.FRONTEND_HOST + '/transactionResult' + `?authorization=${token}`)
    } else {
      res.send('success')
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router
