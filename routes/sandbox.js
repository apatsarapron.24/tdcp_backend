/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
const { Admin, Invoices, Transaction, Invoices_ERP, Report_Problem, Company, Payer, PayOff } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const nodemailer = require('nodemailer')
const { rmSync } = require('fs')
const  moment  = require('moment')
const gen = require('./gen')
const fs = require('fs')

//sendbox receiver:directcredit
router.post('/sendbox/directcredit', async (req, res, next) => {
  try {
    let list_order_no = []
    let sendbox_data = req.body.data
    console.log('sendbox_data')
    console.log(sendbox_data)
    for (let data of sendbox_data) {
      list_order_no.push(data.orderid)
    }
      console.log(list_order_no)
    let sendbox_direct = await db.sequelize.query(
        "select p.payment_no, p.order_id, p.status_code, p.status_message, p.status_payoff "+
        "from PayOffs as p "+
        "where order_id in (:list_order_no) and p.status_payoff = 'available' and p.status_code !=1 ",
        {
          replacements: { list_order_no: list_order_no },
          type: QueryTypes.SELECT
        })
    console.log(sendbox_direct)
    res.send({status: 'Success', data:sendbox_direct})                             
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

//sendbox receiver:ewht
router.post('/sendbox/ewht', async (req, res, next) => {
  try {
    let list_payment_no = []
    let sendbox_data_ewht = req.body.data
    console.log('sendbox_data_ewht')
    console.log(sendbox_data_ewht)
    for (let data of sendbox_data_ewht) {
      list_payment_no.push(data.payment_no)
    }
      console.log(list_payment_no)
    let sendbox_ewht = await db.sequelize.query(

        "select p.order_id, w.payment_no, w.rd_ref, w.process, w.status, w.status_wht "+
        "from WHTs as w "+
        "left join PayOffs as p on w.payment_no = p.payment_no "+
        "where w.payment_no in (:list_payment_no) and w.status_wht = 'available' "+
        "and (w.status !='success' or w.status is null) ",
        {
          replacements: { list_payment_no: list_payment_no },
          type: QueryTypes.SELECT
        })
    if(sendbox_ewht.length != 0){
      res.send({status: 'Success', data:sendbox_ewht})
    }else{
      res.send({status: 'no ewht'})
    }
       
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


//sendbox rd:directcredit
router.post('/sendbox/rd/directcredit', async (req, res, next) => {
  try {
    let list_payment_no = []
    let data_rd_directcredit = req.body.data
    console.log('data_rd_directcredit')
    console.log(data_rd_directcredit)
    for (let data of data_rd_directcredit) {
      list_payment_no.push(data.payment_no)
    }
      console.log(list_payment_no)
    let sendbox_rd_directcerdit = await db.sequelize.query(
        "select p.payment_no, p.status_code, p.status_message, p.tranfer_type "+
        "from PayOffs as p "+
        "where p.payment_no in (:list_payment_no) and p.status_code !=1",
        {
          replacements: { list_payment_no: list_payment_no },
          type: QueryTypes.SELECT
        })
    console.log(sendbox_rd_directcerdit)
    res.send({status: 'Success', data:sendbox_rd_directcerdit})   
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

//sendbox rd:ewht
router.post('/sendbox/rd/ewht', async (req, res, next) => {
  try {
    let list_rd_ref = []
    let data_rd_ewht = req.body.data
    console.log('data_rd_ewht')
    console.log(data_rd_ewht)
    for (let data of data_rd_ewht) {
      list_rd_ref.push(data.rd_ref)
    }
      console.log(list_rd_ref)
    let sendbox_rd_ewht = await db.sequelize.query(
        "select w.rd_ref, w.process, w.status, w.description "+
        "from WHTs as w "+
        "where w.rd_ref in (:list_rd_ref) and w.status_wht = 'available' ",
        {
          replacements: { list_rd_ref: list_rd_ref },
          type: QueryTypes.SELECT
        })
    console.log(sendbox_rd_ewht)
    res.send({status: 'Success', data:sendbox_rd_ewht})   
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})
module.exports = router
