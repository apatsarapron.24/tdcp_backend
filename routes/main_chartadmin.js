/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
const { Admin, Invoices, Transaction, Invoices_ERP, Report_Problem, Company, Payer, PayOff } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const nodemailer = require('nodemailer')
const { rmSync } = require('fs')
const  moment  = require('moment')
const gen = require('./gen')
const fs = require('fs')

function getLstDayOfMonFnc(date) {
    return new Date(date.getFullYear(), date.getMonth(), 0).getDate()
}

router.post("/chart_admin", async (req, res, next) => {
    try {
      let token = req.headers.authorization
      let validate_token = authen.validate_token(token)
      var payer_tax_id = validate_token.source.sub
      var filter_admin = validate_token.source.sub
      if (validate_token.status === 'invalid token') {
        res.send(401)
      } else {

      // "where po.payer_tax_id = :payer_tax_id and w.status_wht = 'available'"+
      var payer_tax = "p.payer_tax_id = '"+ payer_tax_id +"' and "
      var data_payer = "and p.payer_tax_id = '"+ payer_tax_id +"' "
      var payer_company =  "and payer_tax_id = '"+ payer_tax_id +"' "
      if(filter_admin == 'All'){
        data_payer =  " "
        payer_company = " "
        payer_tax = " "
      }


      let datetime = req.body.data
      var startdate = datetime.startdate
      var enddate = datetime.enddate
      // var payer_tax_id = '0107544000094'
      //check filterdate&default
      if (startdate && enddate) {
        console.log('filter')
        var start_date = startdate
        var end_date = enddate + ' 23:59:59.999'
        var month_start = start_date
        var month_end = end_date
        var data_month_end = enddate
      }
      else {
        // last 7 day
        // var date_last_7 = moment().subtract(7, 'days').format('YYYY-MM-DD');
        // var start_date = date_last_7
      
        // var date_now = moment().format('YYYY-MM-DD');
        // var end_date = date_now
      
        //last 12 month
        var default_month_end = moment().subtract(1, 'month').format('YYYY-MM-DD');
        var split_month_end= default_month_end.split('-')
        var total_day_month = getLstDayOfMonFnc(new Date(split_month_end[0], split_month_end[1], split_month_end[2]))
  
        var data_month_end = moment().subtract(1, 'month').format('YYYY-MM');
        var data_month_start = moment().subtract(12, 'month').format('YYYY-MM');
  
        var month_start = data_month_start+'-01'
        var month_end = data_month_end+"-"+total_day_month
        var data_month_end = month_end
      }
    // --------- tab total , inprocess, reject, compress --------------
  
    // ------- total_transaction -------
    var data_transaction = await db.sequelize.query(
          "select count(*) as total_transaction "+
          "from PayOffs as p "+
          "where p.tranfer_type = 'Receiver' "+
          data_payer +
          "and updatedAt >= :month_start and updatedAt <= :month_end",
          {
            replacements: {month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
            type: QueryTypes.SELECT
          })
      for (let data of data_transaction) {
        var transaction = data.total_transaction
      }
      console.log(data_transaction)
  
    // -------- total inprocess---------
    var data_inprocess = await db.sequelize.query(
        "select ( "+
        "count(case when p.tranfer_type = 'Receiver' and p.status_code='000' then 'step1' end) + "+
        "count(case when w.process = 'In process' then 'step2' end) + "+
        "count(case when p.tranfer_type = 'RD' and p.status_code='000' then 'step3' end) + "+
        "count(case when rd_po.status_code = '1' and w.process = 'validated' and w.status = 'success' then 'step4' end) "+
        ") as total_inprocess "+
        "from PayOffs as p "+
        "left join WHTs as w on p.payment_no = w.payment_no "+
        "left join PayOffs as rd_po on w.rd_ref = rd_po.payment_no "+
        "where "+payer_tax +
        " (p.updatedAt >= :month_start and p.updatedAt <= :month_end "+
        " or w.updatedAt >= :month_start and w.updatedAt <= :month_end) ",
        {
          replacements: {month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
          type: QueryTypes.SELECT
        })
    for (let data of data_inprocess) {
      var inprocess = data.total_inprocess
    }
      
    // -------- total reject---------
    var data_reject = await db.sequelize.query(
        "select ( "+
        "count(case when p.tranfer_type = 'Receiver' and p.status_code='2' then 'step1' end)+ "+
        "count(case when w.process = 'validated' and w.status = 'fail' then 'step2' end)+ "+
        "count(case when w.process = 'validated' and w.status = 'success' and rd_po.status_code = '2' then 'step3' end)) as total_reject "+
        "from PayOffs as p "+
        "left join WHTs as w on p.payment_no = w.payment_no "+
        "left join PayOffs as rd_po on w.rd_ref = rd_po.payment_no "+
        "where "+ payer_tax +
        " (p.updatedAt >= :month_start and p.updatedAt <= :month_end "+
        " or w.updatedAt >= :month_start and w.updatedAt <= :month_end) ",
        {
          replacements: {month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
          type: QueryTypes.SELECT
        })
    for (let data of data_reject) {
      var reject = data.total_reject
    }
  
    // -------- total complete---------
    var data_complete = await db.sequelize.query(
        "select ( "+
        "count(case when t.wht_amount = '0' and p.status_code = '1' then 'nowht' end)+ "+
        "count(case when t.wht_amount != '0' and w.process = 'reconciled' and w.status = 'success' then 'wht' end)) "+
        "as total_completed "+
        "from PayOffs as p "+
        "left join Transactions as t on p.order_id = t.order_id "+
        "left join WHTs as w on p.payment_no = w.payment_no "+
        "where "+payer_tax+
        " (p.updatedAt >= :month_start and p.updatedAt <= :month_end "+
        " or w.updatedAt >= :month_start and w.updatedAt <= :month_end) ",
        {
          replacements: {month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
          type: QueryTypes.SELECT
        })
    for (let data of data_complete) {
      var complete = data.total_completed
    }
    // ------- end tab total , inprocess, reject, compress ------------
  
    // -------------------- chart pv month ---------------------
    var data_chart_pv = await db.sequelize.query(
        "select (case when DATEPART(MONTH, tp.createdAt) = 1 then  'ม.ค.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 2 then  'ก.พ.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 3 then  'มี.ค.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 4 then  'เม.ย.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 5 then  'พ.ค.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 6 then  'มิ.ย.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 7 then  'ก.ค.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 8 then  'ส.ค.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 9 then  'ก.ย.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 10 then 'ต.ค.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 11 then 'พ.ย.' "+
        "             when DATEPART(MONTH, tp.createdAt) = 12 then 'ธ.ค.' "+
        "       end) +'/'+SUBSTRING( CONVERT(varchar(10),DATEPART(YEAR, tp.createdAt)+543) , 3, 2) as pv_month, "+
        "       count(tp.order_id) as total_day   "+
        "from Transaction_Payments tp "+
    
        "left join PayOffs p on tp.order_id = p.order_id  "+
        "where tp.updatedAt >= :month_start and tp.updatedAt < :month_end and "+
        "       p.tranfer_type = 'Receiver' "+
        data_payer +
        "group by DATEPART(MONTH, tp.createdAt),DATEPART(YEAR, tp.createdAt) ",
    
        {
          replacements: { month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
          type: QueryTypes.SELECT
        })
    // ----------------- end chart pv day -------------------

    // -------------------- chart_maxamount ---------------------
    var data_chart_maxamount = await db.sequelize.query(
      "select top (5) c.company_id, "+
      "sum(t.amount_exc_vat + t.vat_amount) as total_amount  "+
      "from PayOffs as p  "+
      "left join Transactions as t on p.order_id = t.order_id "+
      "left join Companies as c on p.payer_tax_id = c.sender_tax_id  "+
      "where p.tranfer_type = 'Receiver'  "+
      "and p.updatedAt >= :month_start and p.updatedAt < :month_end "+
      data_payer +
      "group by p.payer_tax_id, c.company_id "+
      "order by total_amount DESC ",

      {
        replacements: { month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
        type: QueryTypes.SELECT
      })
    // ----------------- chart_maxamount -------------------

    // -------------------- end chart_paymentmethod ---------------------
    var data_chart_paymentmethod = await db.sequelize.query(
      "select "+
      "       (case when bank_name = 'SCB' and payment_method = 'AC' then 'SCB'  "+
      "       when bank_name = 'KTB' and payment_method = 'AC' then 'KTB' "+
      "       when bank_name = 'BAY' and payment_method = 'AC' then 'BAY' "+
      "       when bank_name = 'BBL' and payment_method = 'AC' then 'BBL' "+
      "       when bank_name = 'BBL' and payment_method = 'QR' then 'QR'  end) as payment, "+
      "count(bank_name) as total "+
      "from Payment_Trackings  "+
      "where updatedAt >= :month_start and updatedAt <  :month_end "+
      payer_company +
      "group by (case when bank_name = 'SCB' and payment_method = 'AC' then 'SCB' "+
      "       when bank_name = 'KTB' and payment_method = 'AC' then 'KTB' "+
      "       when bank_name = 'BAY' and payment_method = 'AC' then 'BAY' "+
      "       when bank_name = 'BBL' and payment_method = 'AC' then 'BBL' "+
      "       when bank_name = 'BBL' and payment_method = 'QR' then 'QR'  end) "+
      "order by total DESC ",

      {
        replacements: { month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
        type: QueryTypes.SELECT
    })
    // ----------------- end chart_paymentmethod -------------------

    // -------------------- chart_maxpv ---------------------
    var data_chart_maxpv = await db.sequelize.query(
      "select top (5) c.company_id as company_name, "+
      "(count(case when t.wht_amount = '0' and p.status_code = '1'  then 'nowht' end)+  "+
      "count(case when t.wht_amount != '0' and w.process = 'reconciled' and w.status = 'success' then 'wht' end)) "+
      "as total_completed "+
      "from PayOffs as p  "+
      "left join Transactions as t on p.order_id = t.order_id "+
      "left join WHTs as w on p.payment_no = w.payment_no "+
      "left join Companies as c on p.payer_tax_id = c.sender_tax_id  "+
      "where p.updatedAt >= :month_start and p.updatedAt < :month_end "+
      data_payer +
      "group by c.company_id "+
      "order by total_completed DESC ",

      {
        replacements: { month_start: month_start, month_end: month_end, payer_taxid: payer_tax_id},
        type: QueryTypes.SELECT
      })
    // ----------------- chart_maxpv -------------------
      res.send({ status: 'Success', chart_user: 'Company',
                                    total_transaction:transaction, total_inprocess:inprocess,
                                    total_reject:reject,total_complete:complete,
                                    chart_pv:data_chart_pv, chart_maxamount:data_chart_maxamount,
                                    chart_maxpv:data_chart_maxpv,
                                    chart_paymentmethod:data_chart_paymentmethod,
                                    month_start:month_start,month_end:data_month_end });
      }
    } catch (error) {
      console.log(error)
        var error_log_name = await gen.log_name()
        const error_console = new console.Console(fs.createWriteStream(error_log_name)); 
        error_console.log(error);
        res.send({ status: 'error', error: error });
    }
  });


module.exports = router;
