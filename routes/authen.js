const jwt = require('jsonwebtoken');
const fs = require('fs'); 

module.exports = {
  create_token: function (source) {
    let cert = fs.readFileSync('eInvoice.key');
    let token = jwt.sign({ sub: source}, cert, { algorithm: 'HS256'});
    return token
    // return cert
  },
  create_token_invoice: function (invoice, total) {
    let cert = fs.readFileSync('eInvoice.key');
    let token = jwt.sign({ sub: invoice, total: total}, cert, { algorithm: 'HS256'});
    return token
    // return cert
  },
  create_token_role: function (id, role) {
    let cert = fs.readFileSync('eInvoice.key');
    let token = jwt.sign({ id: id, role: role}, cert, { algorithm: 'HS256'});
    return token
  },
  create_token_roleTaxid: function (id, role, taxid) {
    let cert = fs.readFileSync('eInvoice.key');
    let token = jwt.sign({ id: id, role: role, sub: taxid}, cert, { algorithm: 'HS256'});
    return token
  },
  create_token_cashier: function (sub, id, desc, amount, url) {
    let cert = fs.readFileSync('eInvoice.key');
    let token = jwt.sign({ sub: sub, id: id, desc: desc, amount: amount, url:url}, cert, { algorithm: 'HS256'});
    return token
  },
  validate_token: function (source) {
    try {
      let cert = fs.readFileSync('eInvoice.key');
      let token = source
      var result = jwt.verify(token, cert, { algorithms: ['HS256'] }, function (err, payload) {
        if (payload) {
          return {status:'valid token', source: payload}
        } else {
          return {status:'invalid token'}
        }
      })
      return result
    } catch (error) {
      // console.log(error.response.status)
      next(error)
    }
  }
};