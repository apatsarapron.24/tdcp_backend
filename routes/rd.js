/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Company, Bank, Payment_Tracking, PayOff, WHT, RD } = require('../models/index')
const Sequelize = require('sequelize')
const db = require('../models')
const { QueryTypes } = require('sequelize')
const Op = Sequelize.Op
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')


router.post('/resend_tranfer_rd', async (req, res, next) => {
  try {
    let token  = req.headers.authorization
    let validate_token = authen.validate_token(token)
    let payer_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401);
    } else {
      console.log('resend_tranfer_rd: req.body')
      console.log(req.body)
      var rd_ref_list = req.body.rd_ref[0]
      console.log('rd_ref_list')
      console.log(rd_ref_list)

      var bank_data = await Bank.findOne({ where: { name: req.body.bank_code } })
      var branch_code = '0'+req.body.account_no.substring(0, 3)

      var rd_old = await RD.findOne({ order: [['id', 'DESC']] })
      await RD.create({
        tax_id: rd_old.tax_id,
        account_no: req.body.account_no,
        account_name: req.body.account_name,
        bank_code: bank_data.code,
        branch_code: branch_code,
        createdBy: req.body.createdBy
      })

      var update_payoff = await db.sequelize.query(
        "update PayOffs "+
        "set receiver_account_no = :account_no, receiver_account_name = :account_name, "+
        "    receiver_bank_code = :bank_code, receiver_branch_code = :branch_code, "+
        "    status_code = '000', status_message = 'In process', transaction_date = null, "+
        "    transaction_time = null, createdBy = :createdBy  "+
        "where payment_no in (:rd_ref) ",
      {
        replacements: { account_no: req.body.account_no, account_name: req.body.account_name,
                        bank_code: bank_data.code, branch_code: branch_code, 
                        createdBy: req.body.createdBy, rd_ref: rd_ref_list },
        type: QueryTypes.SELECT
      })      


      var rd_payoff = await db.sequelize.query(
      "select * FROM PayOffs where payment_no in (:rd_ref) ",
      {
        replacements: { rd_ref: rd_ref_list },
        type: QueryTypes.SELECT
      })

      var receiver_data = []
      for (let data of rd_payoff) {
        var temp_receiver_date = {}
        Object.assign(temp_receiver_date, {
          'transaction_ref': data.payment_no,
          'name': data.receiver_account_name,
          'tax_id': data.receiver_tax_id,
          'account_no': data.receiver_account_no,
          'bank_code': data.receiver_bank_code,
          'branch_code': data.receiver_branch_code,
          'amount': String(data.pay_amount)
        })
        receiver_data.push(temp_receiver_date)
      }

      //call api rd:directcredit
      var sender_data = await Company.findOne()
      var direct_credit_data = {
        sender_info : {
          id: sender_data.company_id,
          name: sender_data.name,
          tax_id: sender_data.sender_tax_id,
          account_no: sender_data.bank_book_no,
          bank_code: sender_data.bank_code,
          branch_code: sender_data.branch_code
        },
         receiver_info: receiver_data 
      }
      console.log('resend: rd_directcredit')
      console.log(direct_credit_data)
      var api_gettoken = await axios.post(process.env.API_HOST+'/rd_directcredit', direct_credit_data)
      
      res.send('Success')
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router
