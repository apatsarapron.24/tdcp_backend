const express = require("express");
const router = express.Router();
const axios = require('axios');
const { Test } = require('../models/index')
const sequelize = require('sequelize')
const { QueryTypes } = require('sequelize')
const Op = sequelize.Op

const gen = require('./gen')
const fs = require('fs');

router.get("/test", async (req, res) => {
  try {
    console.log('test')
    res.send({ status: 'test',});
  } catch (error) {
    console.log(error)
  	res.send({ status: 'test error', error: error });
  }
});


module.exports = router;
