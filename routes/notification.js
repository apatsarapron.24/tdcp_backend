/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Payment_Tracking, Company, Transaction_Payment, PayOff, WHT } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const axios = require('axios')
const authen = require('./authen')
const nodemailer = require('nodemailer')
const gen = require('./gen')
const fs = require('fs')

var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
var smtpTransport = nodemailer.createTransport(smtp)


router.post('/notification/email/payment/receiver', async (req, res, next) => {
  try { 
    console.log('/notification/email/payment/receiver')
    // console.log(req.body)
    // var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
    // var smtpTransport = nodemailer.createTransport(smtp)
    let mailOptions = {
      from: 'no-reply@thaidotcompayment.co.th',
      to: req.body.receiver_email,
      subject: 'แจ้งเตือนการรับชำระเงิน สำหรับบริการ INET e-Payment',
      html: '<html><br>' +
      'เรียน '+req.body.receiver_name+'<br><br>'+
      'ท่านมียอดรับชำระเงินผ่านระบบ INET e-Payment จาก '+ req.body.payer_name +'<br><br>'+
      'ผู้ให้บริการได้รับชำระเงินภาษี หัก ณ​ ที่จ่าย โดยมีรายละเอียดดังนี้<br><br>'+
      'เลขประจาตัวผู้เสียภาษีอากรของผู้มีหน้าที่นำส่งเงินภาษี : '+req.body.payer_taxid+'<br>'+
      'เลขประจาตัวผู้เสียภาษีอากรของผู้ถูกหักภาษี ณ ที่จ่าย : '+req.body.receiver_taxid+'<br>'+
      'ประเภทของเงินได้พึงประเมิน : '+req.body.income_type+'<br>'+
      'จำนวนเงินที่ชำระไปยังผู้รับเงิน : '+req.body.amount+'<br>'+
      'จำนวนเงินภาษีหัก ณ ที่จ่าย : '+req.body.wht_amount+'<br>'+
      'จำนวนภาษีมูลค่าเพิ่ม : '+req.body.vat_amount+'<br>'+
      'หมายเลขอ้างอิง : '+req.body.rd_ref+'<br><br>'+
      'หากท่านต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อ Call center 02-xxx-xxxx<br><br>'+
      'ขอแสดงความนับถือ<br>'+
      'บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน)'+
      '</html>'
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
      smtpTransport.close()
      let email_status = 'success'
      if (error) {
        console.log('send email error: ' + error)
        email_status = String(error)
        res.send(error)
      } else {
        console.log('email_status: ' + email_status)
        res.send('Success')
      }
    })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/notification/email/payment/payer', async (req, res, next) => {
  try { 
    console.log('/notification/email/payment/payer')
    // console.log(req.body.payer_email)
    // var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
    // var smtpTransport = nodemailer.createTransport(smtp)
    let mailOptions = {
      from: 'no-reply@thaidotcompayment.co.th',
      to: req.body.payer_email,
      subject: 'แจ้งเตือนการรับชำระเงิน สำหรับบริการ INET e-Payment',
      html: '<html><br>' +
      'เรียน '+req.body.payer_name+'<br><br>'+
      'ขอบคุณสำหรับการใช้งานบริการการชำระเงินออนไลน์ผ่านทาง INET e-Payment<br><br>'+
      'ผู้ให้บริการได้รับเงินสำหรับบริการชำระเงิน พร้อมภาษีมูลค่าเพิ่ม และภาษีหัก ณ ที่จ่าย โดยมีรายละเอียดดังนี้<br><br>'+
      'เลขประจาตัวผู้เสียภาษีอากรของผู้มีหน้าที่นำส่งเงินภาษี : '+req.body.payer_taxid+'<br>'+
      'เลขประจาตัวผู้เสียภาษีอากรของผู้ถูกหักภาษี ณ ที่จ่าย : '+req.body.receiver_taxid+'<br>'+
      'ประเภทของเงินได้พึงประเมิน : '+req.body.income_type+'<br>'+
      'จำนวนเงินที่ชำระไปยังผู้รับเงิน : '+req.body.amount+'<br>'+
      'จำนวนเงินภาษีหัก ณ ที่จ่าย : '+req.body.wht_amount+'<br>'+
      'จำนวนภาษีมูลค่าเพิ่ม : '+req.body.vat_amount+'<br>'+
      'หมายเลขอ้างอิง : '+req.body.rd_ref+'<br><br>'+
      'หากท่านต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อ Call center 02-xxx-xxxx<br><br>'+
      'ขอแสดงความนับถือ<br>'+
      'บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน)'+
      '</html>'
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
      smtpTransport.close()
      let email_status = 'success'
      if (error) {
        console.log('send email error: ' + error)
        email_status = String(error)
        res.send(error)
      } else {
        console.log('email_status: ' + email_status)
        res.send('Success')
      }
    })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/notification/email/payment/payer/summary', async (req, res, next) => {
  try { 
    console.log('/notification/email/payment/payer/summary')
    // console.log(req.body.payer_email)
    // var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
    // var smtpTransport = nodemailer.createTransport(smtp)
    let mailOptions = {
      from: 'no-reply@thaidotcompayment.co.th',
      to: req.body.payer_email,
      subject: 'แจ้งเตือนการรับชำระเงิน สำหรับบริการ INET e-Payment',
      html: '<html><br>' +
      'เรียน '+req.body.payer_name+'<br><br>'+
      'ขอบคุณสำหรับการใช้งานบริการการชำระเงินออนไลน์ผ่านทาง INET e-Payment<br><br>'+
      'ผู้ให้บริการได้รับเงินสำหรับบริการชำระเงิน พร้อมภาษีมูลค่าเพิ่ม และภาษีหัก ณ ที่จ่าย (ถ้ามี) โดยมีรายละเอียดดังนี้<br><br>'+
      
      'ชื่อผู้จ่าย : '+req.body.payer_name+'<br>'+
      'เลขประจำตัวผู้เสียภาษีอากรของผู้จ่าย : '+req.body.payer_taxid+'<br>'+
      'จำนวนรายการ : '+req.body.items+'<br>'+
      'จำนวนเงินที่ชำระไปยังผู้รับเงิน : '+req.body.amount+'<br>'+

      'จำนวนเงินภาษีหัก ณ ที่จ่าย : '+req.body.wht_amount+'<br>'+
      'จำนวนภาษีมูลค่าเพิ่ม : '+req.body.vat_amount+'<br>'+
      'จำนวนเงินรวม : '+req.body.pay_amount+'<br>'+
      'หมายเลขอ้างอิง : '+req.body.rd_ref+'<br><br>'+

      'หากท่านต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อ Call center 02-xxx-xxxx<br><br>'+
      'ขอแสดงความนับถือ<br>'+
      'บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน)'+
      '</html>'
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
      smtpTransport.close()
      let email_status = 'success'
      if (error) {
        console.log('send email error: ' + error)
        email_status = String(error)
        res.send(error)
      } else {
        console.log('email_status: ' + email_status)
        res.send('Success')
      }
    })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/notification/email/ewht', async (req, res, next) => {
  try { 
    console.log('/notification/email/ewht')
    // console.log(req.body)
    // var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
    // var smtpTransport = nodemailer.createTransport(smtp)
    let mailOptions = {
      from: 'no-reply@thaidotcompayment.co.th',
      to: req.body.to,
      subject: 'แจ้งเตือนการส่งข้อมูลรายการภาษี หมายเลขรายการนำจ่าย: '+req.body.pv_no,
      html: '<html><br>' +
      'เรียน '+req.body.user+'<br><br>'+
      'ขอบคุณสำหรับการใช้งานบริการการชำระเงินออนไลน์ผ่านทาง INET e-Payment<br><br>'+
      'ระบบได้นำส่งข้อมูลรายการภาษี<br>'+
      'วัน เวลา: '+req.body.date_time+'<br>'+
      'จำนวนเงินไม่รวมภาษีมูลค่าเพิ่ม: '+req.body.amount+'<br>'+
      'จำนวนเงินภาษีมูลค่าเพิ่ม: '+req.body.vat+'<br>'+
      'จำนวนเงินภาษีหัก ณ ที่จ่าย: '+req.body.wht+'<br>'+
      'หมายเลขรายการนำจ่าย: '+req.body.pv_no+'<br>'+
      'หมายเลขอ้างอิงของกรมสรรพากร: '+req.body.rd_ref+'<br>'+
      'ชื่อบัญชีของผู้รับเงิน: '+req.body.account_name+'<br>'+
      'เลขที่บัญชีของผู้รับเงิน: '+req.body.account_no+'<br>'+
      'ธนาคารของผู้รับเงิน: '+req.body.bank+'<br>'+
      'เงื่อนไขของภาษีหัก ณ ที่จ่าย: '+req.body.wht_condition+'<br>'+
      'ประเภทของรายได้: '+req.body.income_type+'<br>'+
      'สถานะ: '+req.body.status+'<br><br>'+
      'สอบถามข้อมูลเพิ่มเติมได้ที่ 02 257 7188 หรือ online-support@inet.co.th<br><br>'+
      'ขอแสดงความนับถือ<br>'+
      'INET e-Payment'+
      '</html>'
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
      smtpTransport.close()
      let email_status = 'success'
      if (error) {
        console.log('send email error: ' + error)
        email_status = String(error)
        res.send(error)
      } else {
        console.log('email_status: ' + email_status)
        res.send('Success')
      }
    })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/notification/email/directcredit', async (req, res, next) => {
  try { 
    console.log('/notification/email/directcredit')
    // console.log(req.body)
    var rd_ref = ''
    if (req.body.rd_ref) {
      rd_ref = 'หมายเลขอ้างอิงของกรมสรรพากร: '+req.body.rd_ref+'<br>'
    }
    // var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
    // var smtpTransport = nodemailer.createTransport(smtp)
    let mailOptions = {
      from: 'no-reply@thaidotcompayment.co.th',
      to: req.body.to,
      subject: 'แจ้งเตือนการโอนเงิน หมายเลขรายการนำจ่าย: '+req.body.pv_no,
      html: '<html><br>' +
      'เรียน '+req.body.user+'<br><br>'+
      'ขอบคุณสำหรับการใช้งานบริการการชำระเงินออนไลน์ผ่านทาง INET e-Payment<br><br>'+
      'ระบบได้โอนเงินไปยัง '+req.body.receiver+'<br>'+
      'วัน เวลา: '+req.body.date_time+'<br>'+
      'จำนวนเงิน: '+req.body.amount+' <br>'+
      'หมายเลขรายการนำจ่าย: '+req.body.pv_no+'<br>'+
      rd_ref+
      'ชื่อบัญชี: '+req.body.account_name+'<br>'+
      'เลขที่บัญชี: '+req.body.account_no+'<br>'+
      'ธนาคาร: '+req.body.bank+'<br>'+
      'สถานะ: '+req.body.status+'<br><br>'+
      'สอบถามข้อมูลเพิ่มเติมได้ที่ 02 257 7188 หรือ online-support@inet.co.th<br><br>'+
      'ขอแสดงความนับถือ<br>'+
      'INET e-Payment'+
      '</html>'
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
      smtpTransport.close()
      let email_status = 'success'
      if (error) {
        console.log('send email error: ' + error)
        email_status = String(error)
        res.send(error)
      } else {
        console.log('email_status: ' + email_status)
        res.send('Success')
      }
    })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/notification/email/directcredit/rd', async (req, res, next) => {
  try { 
    console.log('/notification/email/directcredit/rd')
    // console.log(req.body)
    // var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
    // var smtpTransport = nodemailer.createTransport(smtp)
    let mailOptions = {
      from: 'no-reply@thaidotcompayment.co.th',
      to: req.body.payer_email,
      subject: 'แจ้งเตือนการโอนเงิน หมายเลขอ้างอิงของกรมสรรพากร: '+req.body.rd_ref,
      html: '<html><br>' +
      'เรียน '+req.body.payer_name+'<br><br>'+
      'ขอบคุณสำหรับการใช้งานบริการการชำระเงินออนไลน์ผ่านทาง INET e-Payment<br><br>'+
      'ระบบได้โอนเงินไปยัง กรมสรรพากร<br>'+
      'วัน เวลา: '+req.body.date_time+'<br>'+
      'จำนวนเงิน: '+req.body.amount+'<br>'+
      'หมายเลขรายการนำจ่าย: '+req.body.pv_no+'<br>'+
      'หมายเลขอ้างอิงของกรมสรรพากร: '+req.body.rd_ref+'<br>'+
      'ชื่อบัญชี: '+req.body.account_name+'<br>'+
      'เลขที่บัญชี: '+req.body.account_no+'<br>'+
      'ธนาคาร: '+req.body.bank+'<br>'+
      'สถานะ: '+req.body.status+'<br><br>'+
      'สอบถามข้อมูลเพิ่มเติมได้ที่ 02 257 7188 หรือ online-support@inet.co.th<br><br>'+
      'ขอแสดงความนับถือ<br>'+
      'INET e-Payment'+
      '</html>'
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
      smtpTransport.close()
      let email_status = 'success'
      if (error) {
        console.log('send email error: ' + error)
        email_status = String(error)
        res.send(error)
      } else {
        console.log('email_status: ' + email_status)
        res.send('Success')
      }
    })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/notification/email/reconciled', async (req, res, next) => {
  try { 
    console.log('/notification/email/reconciled')
    // console.log(req.body)
    // var smtp = { host: 'mgw1.online.inet.co.th', port: 25, auth: { user: 'no-reply@thaidotcompayment.co.th', pass: 'Inet@2018' } }
    // var smtpTransport = nodemailer.createTransport(smtp)
    let mailOptions = {
      from: 'no-reply@thaidotcompayment.co.th',
      to: req.body.to,
      subject: 'แจ้งเตือนการนำส่งภาษี หมายเลขรายการนำจ่าย: '+req.body.pv_no,
      html: '<html><br>' +
      'เรียน '+req.body.user+'<br><br>'+
      'ขอบคุณสำหรับการใช้งานบริการการชำระเงินออนไลน์ผ่านทาง INET e-Payment<br><br>'+
      'ระบบได้นำส่งภาษี<br>'+
      'ผู้สั่งจ่าย: '+req.body.payer+'<br>'+
      'เลขประจำตัวผู้เสียภาษีอากรของผู้จ่าย: '+req.body.payer_tax_id+'<br>'+
      'ผู้รับเงิน: '+req.body.receiver+'<br>'+
      'เลขประจำตัวผู้เสียภาษีอากรของผู้รับ: '+req.body.receiver_tax_id+'<br>'+
      'วัน เวลา: '+req.body.date_time+'<br>'+
      'จำนวนเงินภาษีหัก ณ ที่จ่าย: '+req.body.wht+'<br>'+
      'จำนวนเงินภาษีมูลค่าเพิ่ม: '+req.body.vat+'<br>'+
      'หมายเลขรายการนำจ่าย: '+req.body.pv_no+'<br>'+
      'หมายเลขอ้างอิงของกรมสรรพากร: '+req.body.rd_ref+'<br>'+
      'เงื่อนไขของภาษีหัก ณ ที่จ่าย: '+req.body.wht_condition+'<br>'+
      'ประเภทของเงินได้พึงประเมิน: '+req.body.income_type+'<br>'+
      'สถานะ: '+req.body.status+'<br><br>'+
      'สอบถามข้อมูลเพิ่มเติมได้ที่ 02 257 7188 หรือ online-support@inet.co.th<br><br>'+
      'ขอแสดงความนับถือ<br>'+
      'INET e-Payment'+
      '</html>'
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
      smtpTransport.close()
      let email_status = 'success'
      if (error) {
        console.log('send email error: ' + error)
        email_status = String(error)
        res.send(error)
      } else {
        console.log('email_status: ' + email_status)
        res.send('Success')
      }
    })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/notification/onechat', async (req, res, next) => {
  try { 
    var request = req.body
    console.log('/notification/onechat')
    // console.log(request)
    let { data: data_onechat } = await axios.post(process.env.API_ONECHAT, 
      {
        to: req.body.oneid,
        bot_id: process.env.BOT_ID,
        type: 'text',
        message: req.body.msg,
        custom_notification: req.body.msg
      },
      { headers: { Authorization: 'Bearer '+process.env.ONECHAT_AUTHOR } }
    )
    // console.log(data_onechat)
    res.send('Success')
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router
