/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Company, Bank, Payment_Tracking, Payer } = require('../models/index')
const Sequelize = require('sequelize')
const db = require('../models')
const { QueryTypes } = require('sequelize')
const Op = Sequelize.Op
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')

router.post('/a', async (req, res, next) => {
  try {
    // var token = req.headers.authorization
    // var validate_token = authen.validate_token(token)
    // var sender_tax_id = validate_token.source.sub
    // if (validate_token.status === 'invalid token') {
    //   res.send(401)
    // } else {
      var name = req.body.name
      var pv = req.body.pv

      await Transaction.update({ 
        updateBy: name,
        updatedAt: new Date()
      },
      { where: { order_id: pv } })

      res.send({ status: 'success' })

    // }// else validate
  } catch (error) {
    res.send(error)
    next(error)
  }
})


router.get('/transactions', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    var payer_tax_id = validate_token.source.sub
    var filter_admin = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {

      var where = " where p.rcode = '0' group by  tp.order_id ) and t.payer_tax_id = '"+ payer_tax_id +"'"
      // var where = "where po.payer_tax_id = '"+ payer_tax_id +"' and po.tranfer_type not like 'RD' and po.status_payoff = 'available' "
      if(filter_admin == 'All'){
        where =  "where p.rcode = '0' group by  tp.order_id ) "
      }

      var income_type = await db.sequelize.query(
        " SELECT CONCAT(code, ' : ', name_th) as income_type FROM Income_Types ", { type: QueryTypes.SELECT }
      )

      var alltransaction = await db.sequelize.query(
        "select t.receiver_company, b2.name as receiver_bankcode, t.*, "+
        "       (t.amount_exc_vat+t.vat_amount) as amount_inc_vat, "+
        "       CONCAT(it.code, ' : ', it.name_th) as income_type, "+
        "       (t.amount_exc_vat+t.vat_amount-t.wht_amount) as pay_amount "+
        "from Transactions as t "+
        "left join Payers as pa on t.payer_tax_id = pa.tax_id "+
        "left join Banks as b2 on t.receiver_bank_code = b2.code "+
        "left join Income_Types as it on it.code = t.income_type "+
        "where t.order_id not in "+
        "      (select tp.order_id from Payment_Trackings as p "+
        "       left join Transaction_Payments as tp on p.id = tp.tracking_id "+
        where +
        " order by t.doc_date ",
        {
          replacements: { payer_tax_id: payer_tax_id },
          type: QueryTypes.SELECT
        }
      )
      res.send({'alltransaction':alltransaction , 'income_type': income_type})
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/update_transaction', async (req, res, next) => {
  try {
    var token = req.headers.authorization
    var validate_token = authen.validate_token(token)
    let payer_tax_id = validate_token.source.sub

    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      console.log(payer_tax_id)
      var data_update = req.body.data
      // for (let data of data_transaction) {
      var data_income = data_update.type_of_income
      var income_type = data_income.split(':')
      console.log('data',data_update)
      console.log(data_update.document_number)
      console.log(income_type[0])

      await Transaction.update({ 
        order_id: data_update.document_number,
        doc_date: data_update.document_date,
        receiver_company: data_update.receiver_company,
        receiver_tax_id: data_update.receiver_tax_id,
        receiver_account_name: data_update.receiver_account_name,
        receiver_account_no: data_update.receiver_account_no,
        receiver_email: data_update.receiver_email,
        amount_exc_vat: data_update.amount,
        vat_amount: data_update.vat,
        wht_amount: data_update.wht,
        wht_rate: data_update.wht_rate,
        detail: data_update.detail,
        income_type: income_type[0],
        createdBy: data_update.createdBy
      },{
        where: { id: data_update.id }
      })
      // }

      res.send('success')

    }// else validate
  } catch (error) {
    res.send(error)
    next(error)
  }
})

router.post('/create_transaction', async (req, res, next) => {
  try {
    var token = req.headers.authorization
    var validate_token = authen.validate_token(token)
    var payer_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      var bank_data = await Bank.findOne({ where: { name: req.body.data.receiver_bank_code } })
      console.log(bank_data.code)
      var data_create = req.body.data
      console.log('transaction', data_create)
        var data_income = data_create.type_of_income
        var income_type = data_income.split(':')
        await Transaction.create({
          order_id: data_create.document_number,
          doc_date: data_create.document_date,
          detail: data_create.detail,
          payer_tax_id: payer_tax_id,
          receiver_company: data_create.receiver_company,
          receiver_tax_id: data_create.receiver_tax_id,
          receiver_account_name: data_create.receiver_account_name,
          receiver_account_no: data_create.receiver_account_no,
          receiver_bank_code: bank_data.code,
          receiver_type: data_create.receiver_type,
          receiver_mobile: data_create.receiver_mobile,
          receiver_email: data_create.receiver_email,
          amount_exc_vat: data_create.amount,
          vat_amount: data_create.vat,
          income_type: income_type[0],
          wht_rate: data_create.wht_rate,
          wht_amount: data_create.wht,
          wht_condition: data_create.wht_condition,
          createdBy: data_create.createdBy
        })
      res.send('success' )
    }// else validate
  } catch (error) {
    res.send(error)
    next(error)
  }
})

router.get('/transactions_2', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    let sender_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      let alltransaction = await Transaction.findAll({
        where: {
          direct_status: '1'
        },
        include: [
          {
            model: Company,
            where: { sender_tax_id: sender_tax_id },
            required: true
          },
          {
            model: Bank,
            attributes: ['code', 'name']
          }
        ]
      })
      // let bankList = await Bank.findAll({
      //   where: {
      //     code: alltransaction.receiver_code
      //   }
      // })
      // alltransaction['bank_name'] = alltransaction.
      res.send(alltransaction)
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.get('/latest_transaction', async (req, res, next) => {
  try {
    let token  = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401);
    } else {
      let ops_payment_ref = validate_token.source.sub
      let transaction = await Payment_Tracking.findOne({
        where: {
          ops_payment_ref: ops_payment_ref
        }
      })
      // res.send(transaction)

      var invoices = await db.sequelize.query( //1
        'select t.order_id, t.receiver_account_name as receiver_company, '+
        '       (t.amount_exc_vat+t.vat_amount) as grand_total '+
        'from Payment_Trackings p ' +
        "left join Transaction_Payments tp on p.id = tp.tracking_id "+
        "left join Transactions t on tp.order_id = t.order_id "+
        'where p.ops_payment_ref = :pay_no ',
        {
          replacements: { pay_no: ops_payment_ref },
          type: QueryTypes.SELECT
        }
      )         

      res.send({ transaction: transaction, invoices: invoices })
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

module.exports = router
