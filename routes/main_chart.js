/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
const { Admin, Invoices, Transaction, Invoices_ERP, Report_Problem, Company, Payer, PayOff } = require('../models/index')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const db = require('../models')
const { QueryTypes } = require('sequelize')
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const nodemailer = require('nodemailer')
const { rmSync } = require('fs')
const  moment  = require('moment')
const gen = require('./gen')
const fs = require('fs')


function getLstDayOfMonFnc(date) {
  return new Date(date.getFullYear(), date.getMonth(), 0).getDate()
}

//main charts user
router.post('/main/charts', async (req, res, next) => {
  try {

    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    var payer_tax_id = validate_token.source.sub
    console.log('payer_tax_id main' )
     console.log(payer_tax_id)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
    
    let datetime = req.body.data
    // for (let data of datetime) {
      var startdate = datetime.startdate
      var enddate = datetime.enddate
      //  var payer_tax_id = '0107544000094'
    // }

    //check filterdate&default
    if (startdate && enddate) {
      console.log('filter')
      var start_date = startdate
      var end_date = enddate + ' 23:59:59.999'
      var month_start = start_date
      var month_end = end_date
      console.log(month_start)
      console.log(month_end)
    }
    else {
  
      // last 7 day
      var date_last_7 = moment().subtract(7, 'days').format('YYYY-MM-DD');
      var start_date = date_last_7
    
      var date_now = moment().format('YYYY-MM-DD');
      var end_date = date_now
    
      //last 12 month
      var default_month_end = moment().subtract(1, 'month').format('YYYY-MM-DD');
      var split_month_end= default_month_end.split('-')
      var total_day_month = getLstDayOfMonFnc(new Date(split_month_end[0], split_month_end[1], split_month_end[2]))

      var data_month_end = moment().subtract(1, 'month').format('YYYY-MM');
      var data_month_start = moment().subtract(12, 'month').format('YYYY-MM');

      var month_start = data_month_start+'-01'
      var month_end = data_month_end+"-"+total_day_month
      
    }


    // --------- tab total , inprocess, reject, compress --------------

    // ------- total_transaction -------
    var data_transaction = await db.sequelize.query(
          "select count(*) as total_transaction "+
          "from PayOffs as p "+
          "where  p.payer_tax_id = :payer_taxid and p.tranfer_type = 'Receiver' "+
          "and updatedAt >= :startdate and updatedAt <= :enddate",
          {
            replacements: {startdate: start_date, enddate: end_date, payer_taxid: payer_tax_id},
            type: QueryTypes.SELECT
          }
    )
    for (let data of data_transaction) {
      var transaction = data.total_transaction
    }

    // -------- total inprocess---------
    var data_inprocess = await db.sequelize.query(
          "select ( "+
          "count(case when p.tranfer_type = 'Receiver' and p.status_code='000' then 'step1' end) + "+
          "count(case when w.process = 'In process' then 'step2' end) + "+
          "count(case when p.tranfer_type = 'RD' and p.status_code='000' then 'step3' end) + "+
          "count(case when rd_po.status_code = '1' and w.process = 'validated' and w.status = 'success' then 'step4' end) "+
          ") as total_inprocess "+
          "from PayOffs as p "+
          "left join WHTs as w on p.payment_no = w.payment_no "+
          "left join PayOffs as rd_po on w.rd_ref = rd_po.payment_no "+
          "where p.payer_tax_id = :payer_taxid "+
          "and (p.updatedAt >= :startdate and p.updatedAt <= :enddate or "+
          "w.updatedAt >= :startdate and w.updatedAt <= :enddate) ",
          {
            replacements: {startdate: start_date,enddate: end_date, payer_taxid: payer_tax_id},
            type: QueryTypes.SELECT
          }
    )
    for (let data of data_inprocess) {
      var inprocess = data.total_inprocess
    }

    // -------- total reject---------
    var data_reject = await db.sequelize.query(
          "select ( "+
          "count(case when p.tranfer_type = 'Receiver' and p.status_code='2' then 'step1' end)+ "+
          "count(case when w.process = 'validated' and w.status = 'fail' then 'step2' end)+ "+
          "count(case when w.process = 'validated' and w.status = 'success' and rd_po.status_code = '2' then 'step3' end)) as total_reject "+
          "from PayOffs as p "+
          "left join WHTs as w on p.payment_no = w.payment_no "+
          "left join PayOffs as rd_po on w.rd_ref = rd_po.payment_no "+
          "where p.payer_tax_id = :payer_taxid "+
          "and (p.updatedAt >= :startdate and p.updatedAt <= :enddate or "+
          "w.updatedAt >= :startdate and w.updatedAt <= :enddate) ",
          {
            replacements: {startdate: start_date,enddate: end_date, payer_taxid: payer_tax_id},
            type: QueryTypes.SELECT
          }
    )
    for (let data of data_reject) {
      var reject = data.total_reject
    }

    // -------- total complete---------
    var data_complete = await db.sequelize.query(
          "select ( "+
          "count(case when t.wht_amount = '0' and p.status_code = '1' then 'nowht' end)+ "+
          "count(case when t.wht_amount != '0' and w.process = 'reconciled' and w.status = 'success' then 'wht' end)) "+
          "as total_completed "+
          "from PayOffs as p "+
          "left join Transactions as t on p.order_id = t.order_id "+
          "left join WHTs as w on p.payment_no = w.payment_no "+
          "where p.payer_tax_id = :payer_taxid "+
          "and (p.updatedAt >= :startdate and p.updatedAt <= :enddate or "+
          "w.updatedAt >= :startdate and w.updatedAt <= :enddate) ",
          {
            replacements: {startdate: start_date,enddate: end_date, payer_taxid: payer_tax_id},
            type: QueryTypes.SELECT
          }
    )
    for (let data of data_complete) {
      var complete = data.total_completed
    }

    // ------- end tab total , inprocess, reject, compress ------------


    // -------------------- chart pv day ---------------------
    var data_chart_pv = await db.sequelize.query(
      "select CONVERT(varchar(10),DATEPART(DAY, tp.updatedAt)) +'/'+  "+
      "       (case when DATEPART(MONTH, tp.updatedAt) = 1  then 'ม.ค.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 2  then 'ก.พ.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 3  then 'มี.ค.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 4  then 'เม.ย.'"+
      "             when DATEPART(MONTH, tp.updatedAt) = 5  then 'พ.ค.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 6  then 'มิ.ย.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 7  then 'ก.ค.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 8  then 'ส.ค.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 9  then 'ก.ย.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 10 then 'ต.ค.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 11 then 'พ.ย.' "+
      "             when DATEPART(MONTH, tp.updatedAt) = 12 then 'ธ.ค.' "+
      "       end ) +'/'+SUBSTRING( CONVERT(varchar(10),DATEPART(YEAR, tp.updatedAt)+543) , 3, 2) as pv_day, "+
      "       count(tp.order_id) as total_day  "+
      "from Transaction_Payments tp "+

      "left join PayOffs p on tp.order_id = p.order_id  "+
      "where tp.updatedAt >= :startdate and tp.updatedAt < :enddate and "+
      "      p.payer_tax_id = :payer_taxid and p.tranfer_type = 'Receiver' "+
      "group by DATEPART(YEAR, tp.updatedAt), DATEPART(MONTH, tp.updatedAt), DATEPART(DAY, tp.updatedAt) ",

      {
        replacements: { startdate: start_date, enddate: end_date, payer_taxid: payer_tax_id },
        type: QueryTypes.SELECT
      }
    )
    // ----------------- end chart pv day -------------------

    // ----------------- chart tax month --------------------
    var chart_taxmonth = await db.sequelize.query(
      "select (case when DATEPART(MONTH, updatedAt) = 1 then  'ม.ค.'"+
      "             when DATEPART(MONTH, updatedAt) = 2 then  'ก.พ.'"+
      "             when DATEPART(MONTH, updatedAt) = 3 then  'มี.ค.'"+
      "             when DATEPART(MONTH, updatedAt) = 4 then  'เม.ย.'"+
      "             when DATEPART(MONTH, updatedAt) = 5 then  'พ.ค.'"+
      "             when DATEPART(MONTH, updatedAt) = 6 then  'มิ.ย.'"+
      "             when DATEPART(MONTH, updatedAt) = 7 then  'ก.ค.'"+
      "             when DATEPART(MONTH, updatedAt) = 8 then  'ส.ค.'"+
      "             when DATEPART(MONTH, updatedAt) = 9 then  'ก.ย.'"+
      "             when DATEPART(MONTH, updatedAt) = 10 then 'ต.ค.'"+
      "             when DATEPART(MONTH, updatedAt) = 11 then 'พ.ย.'"+
      "             when DATEPART(MONTH, updatedAt) = 12 then 'ธ.ค.'"+
      "         end) +'/'+SUBSTRING( CONVERT(varchar(10),DATEPART(YEAR, updatedAt)+543) , 3, 2) as date, "+
      "         count(updatedAt) as amount "+
      "from WHTs "+
      "where process = 'reconciled' and status = 'success' and "+
      "      updatedAt >= :month_start and updatedAt < :month_end  "+
      "      and payer_tax_id = :payer_tax_id "+
      "GROUP BY DATEPART(YEAR, updatedAt), DATEPART(MONTH, updatedAt) ",
      {
        replacements: {month_start: month_start, month_end: month_end, payer_tax_id: payer_tax_id },
        type: QueryTypes.SELECT
      }
    )
    // ----------------- end chart tax month ----------------
      
    // ------------------- chart_expenses ------------------- 
    var chart_expenses = await db.sequelize.query(
      "select top (5) t.receiver_company, "+
      "sum(t.amount_exc_vat + t.vat_amount) as total_amount "+
      "from PayOffs as p "+
      "left join Transactions as t on p.order_id = t.order_id "+
      "where p.updatedAt >= :month_start and p.updatedAt < :month_end "+
      "and p.tranfer_type = 'Receiver' "+
      "and p.payer_tax_id  = :payer_tax_id "+
      "group by p.receiver_tax_id, t.receiver_company "+
      "order by total_amount DESC ",
      {
        replacements: {month_start: month_start, month_end: month_end, payer_tax_id: payer_tax_id },
        type: QueryTypes.SELECT
      }
    )
    // --------------- end chart_expenses ------------------- 

    // ------------------- charts_monthly ------------------- 
    // vendor
    var charts_monthly_vendor = await db.sequelize.query(
      "select "+
      "     (case when DATEPART(MONTH, p.updatedAt) = 1 then  'ม.ค.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 2 then  'ก.พ.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 3 then  'มี.ค.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 4 then  'เม.ย.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 5 then  'พ.ค.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 6 then  'มิ.ย.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 7 then  'ก.ค.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 8 then  'ส.ค.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 9 then  'ก.ย.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 10 then 'ต.ค.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 11 then 'พ.ย.' "+
      "     when DATEPART(MONTH, p.updatedAt) = 12 then 'ธ.ค.' "+
      "   end) +'/'+SUBSTRING( CONVERT(varchar(10),DATEPART(YEAR, p.updatedAt)+543) , 3, 2) as date, "+
      "   sum(p.pay_amount) as total_vendor "+
      "from PayOffs as p  "+
      "where p.tranfer_type = 'Receiver' and p.payer_tax_id = :payer_tax_id "+
      "and updatedAt >= :month_start and updatedAt < :month_end "+
      "group by DATEPART(YEAR, p.updatedAt), DATEPART(MONTH, p.updatedAt) ",
      {
        replacements: {month_start: month_start, month_end: month_end, payer_tax_id: payer_tax_id },
        type: QueryTypes.SELECT
      }
    )

    //RD
    var charts_monthly_rd = await db.sequelize.query(
      "select "+
      "     (case when DATEPART(MONTH, w.updatedAt) = 1 then  'ม.ค.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 2 then  'ก.พ.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 3 then  'มี.ค.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 4 then  'เม.ย.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 5 then  'พ.ค.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 6 then  'มิ.ย.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 7 then  'ก.ค.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 8 then  'ส.ค.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 9 then  'ก.ย.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 10 then 'ต.ค.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 11 then 'พ.ย.' "+
      "     when DATEPART(MONTH, w.updatedAt) = 12 then 'ธ.ค.' "+
      "   end) +'/'+SUBSTRING( CONVERT(varchar(10),DATEPART(YEAR, w.updatedAt)+543) , 3, 2) as date, "+
      "   sum(w.wht_amount) as total_rd "+
      "from WHTs as w  "+
      "where w.payer_tax_id = :payer_tax_id "+
      "and updatedAt >= :month_start and updatedAt < :month_end  "+
      "group by DATEPART(YEAR, w.updatedAt), DATEPART(MONTH, w.updatedAt) ",
      {
        replacements: {month_start: month_start, month_end: month_end, payer_tax_id: payer_tax_id },
        type: QueryTypes.SELECT
      }
    )
    // --------------- end chart_expenses ------------------- 

    res.send({status: 'Success', total_transaction:transaction, total_inprocess:inprocess, 
                                 total_reject:reject,total_complete:complete,
                                 chart_pv:data_chart_pv,
                                 chart_taxmonth: chart_taxmonth,
                                 chart_expenses: chart_expenses,
                                 chart_monthly_vendor:charts_monthly_vendor,
                                 chart_monthly_rd:charts_monthly_rd

                      }) 
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

//main chart_tax user
router.post('/main/charts_tax_user', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    var payer_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
    

    let datetime = req.body.data
    // for (let data of datetime) {
      var years = datetime.years
     // var payer_tax_id = '0107544000094'
    // }

    //check filterdate&default
    if (years) {
      var start_year = years+'-01-01'
      var end_year = years+'-12-31 23:59:59.999'
    }
    else {
      //last 12 month
      var default_month_end = moment().subtract(1, 'month').format('YYYY-MM-DD');
      var split_month_end= default_month_end.split('-')
      var total_day_month = getLstDayOfMonFnc(new Date(split_month_end[0], split_month_end[1], split_month_end[2]))

      var data_month_end = moment().subtract(1, 'month').format('YYYY-MM');
      var data_month_start = moment().subtract(12, 'month').format('YYYY-MM');

      var start_year = data_month_start+'-01'
      var end_year = data_month_end+"-"+total_day_month
      
    }

    // ------------------- chart_incometype ------------------- 
    var chart_incometype = await db.sequelize.query(
      "select ( "+
            "case when t.receiver_type = 'personal' and (w.income_type = '001' or w.income_type = '002') then 'ภงด 1' "+

            "when t.receiver_type = 'personal' and (w.income_type = '003' or w.income_type = '004' or w.income_type = '005' or "+
            "w.income_type = '006' or w.income_type = '007' or w.income_type = '008' or w.income_type = '009' or "+
            "w.income_type = '010' or w.income_type = '011') then 'ภงด 2'  "+

            "when t.receiver_type = 'personal' and (w.income_type = '012' or w.income_type = '013' or w.income_type = '014' or "+
            "w.income_type = '015' or w.income_type = '016' or w.income_type = '017' or w.income_type = '018' or "+
            "w.income_type = '019' or w.income_type = '020' or  w.income_type = '021' or  w.income_type = '022' or "+
            "w.income_type = '023') then 'ภงด 3' "+

            "when t.receiver_type = 'company' and (w.income_type = '002' or w.income_type = '003' or w.income_type = '004' or "+
            "w.income_type = '005' or w.income_type = '006' or w.income_type = '007' or w.income_type = '008' or "+
            "w.income_type = '009' or w.income_type = '010' or  w.income_type = '011' or w.income_type = '012' or "+
            "w.income_type = '013' or w.income_type = '014' or  w.income_type = '015' or w.income_type = '016' or "+
            "w.income_type = '017' or w.income_type = '018' or  w.income_type = '019' or w.income_type = '020' or "+
            "w.income_type = '021' or  w.income_type = '022' or w.income_type = '023') then 'ภงด 53' "+
            "end) as type_rd ,sum(w.wht_amount) as total_amount "+

      "from PayOffs as p "+
      "left join Transactions as t on p.order_id = t.order_id "+
      "left join WHTs as w on p.payment_no = w.payment_no "+
      "where  w.payer_tax_id = :payer_tax_id "+
      "and w.updatedAt >= :start_year and w.updatedAt < :end_year "+
      "group by ( "+
            "case when t.receiver_type = 'personal' and (w.income_type = '001' or w.income_type = '002') then 'ภงด 1' "+

            "when t.receiver_type = 'personal' and (w.income_type = '003' or w.income_type = '004' or w.income_type = '005' or "+
            "w.income_type = '006' or w.income_type = '007' or w.income_type = '008' or w.income_type = '009' or "+
            "w.income_type = '010' or w.income_type = '011') then 'ภงด 2' "+

            "when t.receiver_type = 'personal' and (w.income_type = '012' or w.income_type = '013' or w.income_type = '014' or "+
            "w.income_type = '015' or w.income_type = '016' or w.income_type = '017' or w.income_type = '018' or "+
            "w.income_type = '019' or w.income_type = '020' or  w.income_type = '021' or  w.income_type = '022' or "+
            "w.income_type = '023') then 'ภงด 3' "+

            "when t.receiver_type = 'company' and (w.income_type = '002' or w.income_type = '003' or w.income_type = '004' or "+
            "w.income_type = '005' or w.income_type = '006' or w.income_type = '007' or w.income_type = '008' or "+
            "w.income_type = '009' or w.income_type = '010' or  w.income_type = '011' or w.income_type = '012' or "+
            "w.income_type = '013' or w.income_type = '014' or  w.income_type = '015' or w.income_type = '016' or "+
            "w.income_type = '017' or w.income_type = '018' or  w.income_type = '019' or w.income_type = '020' or "+
            "w.income_type = '021' or  w.income_type = '022' or w.income_type = '023') then 'ภงด 53' "+
            "end)",

      {
        replacements: {start_year: start_year, end_year: end_year, payer_tax_id: payer_tax_id },
        type: QueryTypes.SELECT
    })

    console.log(chart_incometype)
    var data_chart_income = chart_incometype
    var total = 0
      for (let data_income of chart_incometype) {
        total = total + data_income.total_amount
      }

      var send_income = {}
        Object.assign(send_income, {
          'type_rd': 'รวม',
          'total_amount': total,
        })
        data_chart_income.push(send_income)
    // console.log(total)
    // console.log(data_chart_incometype)
    // --------------- end chart_incometype -------------------

    // ----------------------- tax_years -----------------------
    var tax_years = await db.sequelize.query(
      "select DATEPART(YEAR, w.updatedAt) as tax_years "+
      "from WHTs as w "+
      "where w.payer_tax_id = :payer_taxid "+
      "group by DATEPART(YEAR, w.updatedAt)",
      {
        replacements: {payer_taxid: payer_tax_id},
        type: QueryTypes.SELECT
      })
    // ------------------- end tax_year -----------------------

    res.send({status: 'Success', chart_income:data_chart_income,
                                 tax_years:tax_years
    }) 
  }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

module.exports = router


// select DATEPART(YEAR, createdAt), DATEPART(MONTH, createdAt), count(createdAt)
// FROM [uat_ewht].[dbo].[WHTs] 
// where process = 'reconciled' and status = 'success' and 
// createdAt >= '2020-09-01' and createdAt < '2021-09-30' and 
// status_wht = 'available' and payer_tax_id = '0107544000094' 
// GROUP BY DATEPART(YEAR, createdAt), DATEPART(MONTH, createdAt)