/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Company, Bank, Payment_Tracking, 
        PayOff, WHT, Wht_Log, RD, 
        Payer, Log_RDdata } = require('../models/index')
const Sequelize = require('sequelize')
const db = require('../models')
const { QueryTypes } = require('sequelize')
const Op = Sequelize.Op
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')


router.get('/tax_info', async (req, res, next) => {
  try {
    let token  = req.headers.authorization
    let validate_token = authen.validate_token(token)
    let payer_tax_id = validate_token.source.sub
    var filter_admin = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401);
    } else {
      var where = "where po.payer_tax_id = '"+ payer_tax_id + "' "
      if(filter_admin == 'All'){
        where =  " "
      }

      // var rd_data = await RD.findOne({ order: [['id', 'DESC']] })
      var rd_data = await db.sequelize.query(
        " select TOP (1) rd.tax_id, rd.account_no, rd.account_name,  "+
        "                b.name as bank_code, rd.branch_code, rd.createdBy "+
        " from RDs rd "+
        " left join Banks b on rd.bank_code = b.code "+
        " order by rd.id desc ",
        {
          type: QueryTypes.SELECT
        }
      ) 

      var income_type = await db.sequelize.query(
        " SELECT CONCAT(code, ' : ', name_th) as income_type FROM Income_Types ", { type: QueryTypes.SELECT }
      )       

      var alltax = await db.sequelize.query(
        "select w.rd_ref as rd_reference, po.payment_no, w.wht_condition, "+
        "       po.order_id, po.transaction_date as payment_date, po.transaction_time as payment_time, "+
        "       t.detail as detail, t.receiver_company as name, "+

        "case when w.validate_transdate is not null then w.validate_transdate else '' end as validate_date, "+
        "case when w.validate_transtime is not null then w.validate_transtime else '' end as validate_time, "+
        "case when rd_po.transaction_date is not null then rd_po.transaction_date else '' end as transfer_date, "+
        "case when rd_po.transaction_time is not null then rd_po.transaction_time else '' end as transfer_time, "+
        "case when w.reconclie_transdate is not null then w.reconclie_transdate else '' end as reconclie_date, "+
        "case when w.reconclie_transtime is not null then w.reconclie_transtime else '' end as reconclie_time, "+

        "case when (w.process = 'validated' or w.process = 'reconciled') and w.status = 'success' then 'Completed' "+ 
        "when w.process = 'validated' and w.status = 'fail' then 'Rejected' "+
        "when po.status_code = '1' then 'In process' "+
        "when w.process is null then '' end as tax_information, "+
        
        "case when rd_po.status_code = '1' and (w.process = 'validated' or w.process = 'reconciled') and  status = 'success' then 'Completed' "+ 
        "when rd_po.status_code = '000' and w.process = 'validated' and w.status = 'success' then 'In process' "+
        "when rd_po.status_code is null or (w.process = 'validated' and w.status = 'fail') or w.process = 'In process' then '' "+
        "else 'Rejected' end as tranfer, "+

        "case when w.process = 'reconciled' and w.status = 'success' then 'Completed' "+
        "when w.process = 'reconciled' and w.status = 'fail' then 'Rejected' "+
        "when rd_po.status_code = '1' and w.process = 'validated' and w.status = 'success' then 'In process' "+
        "else '' end as reconcile, "+ 

        "w.receiver_tax_id, po.receiver_account_no, b.name as receiver_bank_code, "+
        "po.receiver_branch_code, t.receiver_email,t.receiver_company, CONCAT(it.code, ' : ', it.name_th) as income_type, "+
        "t.amount_exc_vat, t.vat_amount, (t.amount_exc_vat + t.vat_amount) as grand_total, "+
        "w.wht_amount, t.wht_rate, w.payer_tax_id "+

        "from WHTs as w "+ 
        "left join PayOffs as po on w.payment_no = po.payment_no  "+
        "left join PayOffs as rd_po on w.rd_ref = rd_po.payment_no  "+
        "left join Transactions as t on po.order_id = t.order_id  "+
        "left join Income_Types as it on it.code = w.income_type "+

        "left join Banks as b on po.receiver_bank_code = b.code "+
        where +
        "order by w.validate_transdate desc, w.payment_no asc ",
        {
          replacements: { payer_tax_id: payer_tax_id },
          type: QueryTypes.SELECT
        }
      )
      res.send({ 'receiver': alltax, 'rd': rd_data[0], 'income_type': income_type })
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})



router.post('/resend_tax_info', async (req, res, next) => {
  try {
    let token  = req.headers.authorization
    let validate_token = authen.validate_token(token)
    let payer_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401);
    } else {
      // console.log('resend_tax_info: body')
      // console.log(req.body)
      var bank_data = await Bank.findOne({ where: { name: req.body.receiver_bank_code } })
      var payoff_data = await PayOff.findOne({ where: { payment_no: req.body.payment_no } })

      var wht_date = await WHT.findOne({ where: { payment_no: req.body.payment_no, rd_ref: req.body.rd_reference } })

      await payoff_data.update({ 
        receiver_tax_id: req.body.receiver_tax_id 
      })
      await Wht_Log.create({
        rd_ref: wht_date.rd_ref,
        payment_no: wht_date.payment_no,
        receiver_tax_id: wht_date.receiver_tax_id,
        wht_condition: wht_date.wht_condition,
        income_type: wht_date.income_type,
        createdBy: wht_date.createdBy
      })
      await wht_date.update({
        receiver_tax_id: req.body.receiver_tax_id,
        wht_condition: req.body.wht_condition,
        income_type: req.body.income_type,
        process: 'In process',
        status: null,
        description: null,
        validate_transdate: null,
        validate_transtime: null,
        reconclie_transdate: null,
        reconclie_transtime: null,        
        createdBy: req.body.createdBy
      })

      var pno_inprocess = await db.sequelize.query(
        " select w.payment_no, w.payer_tax_id, w.receiver_tax_id, "+
        "        p.receiver_account_name, p.receiver_account_no, p.receiver_branch_code, "+
        "        (t.amount_exc_vat+t.vat_amount) as amount, t.vat_amount, "+
        "        w.wht_amount, w.wht_condition, w.income_type, "+
        "        p.transaction_date as payment_date, p.transaction_time as payment_time "+
        " from WHTs as w "+
        " left join PayOffs as p on w.payment_no = p.payment_no "+
        " left join Transactions as t on p.order_id = t.order_id "+
        " where w.rd_ref = :rd_ref and (w.process = 'validated' and  w.status = 'success') or w.process = 'In process' ",
        {
          replacements: { rd_ref: req.body.rd_reference },
          type: QueryTypes.SELECT
        }
      )   
      var pno_available = await db.sequelize.query(
        " select count(payment_no) as count from WHTs where rd_ref = :rd_ref ",
        {
          replacements: { rd_ref: req.body.rd_reference },
          type: QueryTypes.SELECT
        }
      )         

      if (pno_inprocess.length == pno_available[0].count) {
        // call api ewht
        var receiver_data = []
        var payer_data = await Payer.findOne({ where: { tax_id: wht_date.payer_tax_id } })
        var sender_data = await Company.findOne()
        var rd_data = await RD.findOne({ order: [['id', 'DESC']] })

        for (let data of pno_inprocess) {
          var temp_receiver_date = {}
          Object.assign(temp_receiver_date, {
            'name': data.receiver_account_name,
            'tax_id': data.receiver_tax_id,
            'account': data.receiver_account_no,
            'bank_code': bank_data.code,
            'branch_code': data.receiver_branch_code,
            'amount': String(data.amount),
            'vat_amount': String(data.vat_amount),
            'wht_amount': String(data.wht_amount),
            'wht_condition': data.wht_condition,
            'type_of_income': data.income_type,
            'payment_reference': data.payment_no,
            'payment_date': data.payment_date,
            'payment_time': data.payment_time
          })
           receiver_data.push(temp_receiver_date)
        }

        var wht_result = {
          sender_info : {
            id: sender_data.company_id,
            name: sender_data.name,
            tax_id: sender_data.sender_tax_id,
            account: sender_data.bank_book_no,
            bank_code: sender_data.bank_code,
            branch_code: sender_data.branch_code
          },
           receiver_info: receiver_data   
        }
        // console.log('receiver_ewht')
        // console.log(wht_result)
        try {
          var log_RDdata = await Log_RDdata.create({ 
            tracking_id: payoff_data.tracking_id, data_type: 'validate', call_data: JSON.stringify(wht_result) 
          })          
          await axios.post(process.env.API_HOST+'/receiver_ewht', wht_result)
          res.send('Success')
        } catch (error) {
          console.log(error)
          var error_log_name = await gen.log_name()
          const error_console = new console.Console(fs.createWriteStream(error_log_name))
          error_console.log(error)
          await log_RDdata.update({ call_data: 'error' })
          res.send({status: 'Error: Receiver Ewht'}) 
        }

      }
      res.send('Success')
    }  
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


module.exports = router
