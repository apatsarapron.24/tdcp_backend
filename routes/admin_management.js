/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
const { User } = require('../models/index')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')


router.get('/admin_list', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      var myList = await User.findAll({
        attributes: ['id', 'email', 'role', 'account_id', 'createBy', 'createdAt', 'updatedAt'],
        order: [['id', 'DESC']]
      })
      res.send(myList)
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/add_admin', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      let email = req.body.email
      var checkDuplicate = await User.findAll({
        attributes: ['email'],
        where: {
          email: email
        }
      })
      // console.log(checkDuplicate)
      if (checkDuplicate.length === 0) {
        await User.create({
          email: email,
          role: req.body.role,
          createBy: req.body.createBy
        })
        res.send('Success')
        // res.send('Success')
      } else {
        res.send('Duplicate Email')
      }
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/edit_admin', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      User.update(
        {
          email: req.body.email, role: req.body.role },
        { returning: true, where: { id: req.body.id } }
      )
      res.send('edit success')
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.post('/del_admin', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else {
      User.destroy({
        where: { id: req.body.id }
      })
      // console.log(updateRole.email)
      res.send('delete success')
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})
module.exports = router
