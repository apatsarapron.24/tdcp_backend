const db = require('../models')
const express = require('express')
const router = express.Router()
const axios = require('axios')
const { Company } = require('../models/index')
const sequelize = require('sequelize')
const { QueryTypes } = require('sequelize')
const Op = sequelize.Op
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')


router.get('/companys_list', async (req, res, next) => {
  try {
    let token = req.headers.authorization
    let taxid = ''
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else { 
      var companys_list = []
      var all = 'All'
      companys_list.push(all)
      let company_data = await Company.findAll({ order: [['name', 'ASC']] })
      if (company_data.length > 0) {
        for (let data of company_data) {
          companys_list.push(data.name)
        }
      }
      res.send(companys_list)
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

router.get('/company_token/:company_name', async (req, res, next) => {
  try {
    let { company_name } = req.params
    let token = req.headers.authorization
    let validate_token = authen.validate_token(token)
    if (validate_token.status === 'invalid token') {
      res.send(401)
    } else { 
      if(company_name == 'All'){
        let new_token =  authen.create_token(company_name)
        res.send(new_token)     
      }else{
        var company_data = await Company.findOne({ where: { name: company_name } })
        let new_token =  authen.create_token(company_data.sender_tax_id)
        res.send(new_token)
      }
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})



module.exports = router
