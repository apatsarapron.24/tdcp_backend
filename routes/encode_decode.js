module.exports = {
  encode: function (source) {
    let buff = new Buffer(source);
    let base64data = buff.toString('base64');
    return base64data 
  },
  encode_object: function (source) {
    let buff = JSON.stringify(source);
    let base64data = Buffer.from(buff).toString("base64");
    return base64data
  },
  decode: function (source) {
    let buff = new Buffer(source, 'base64');
    let text = buff.toString('utf-8');    
    let array =  text.split(' ');
    return array
  },
  decode_object: function (source) {
    let buff = new Buffer(source, 'base64');
    let base64data2 = buff.toString('utf-8');
    let json = JSON.parse(base64data2)
    return json
  }
};