/* eslint-disable camelcase */
const express = require('express')
const router = express.Router()
// eslint-disable-next-line camelcase
const { Transaction, Company, Bank, Payment_Tracking, 
        PayOff, Payoff_Log, Payer, Log_PayDC } = require('../models/index')
const Sequelize = require('sequelize')
const db = require('../models')
const { QueryTypes } = require('sequelize')
const Op = Sequelize.Op
const encode_decode = require('./encode_decode')
const axios = require('axios')
const authen = require('./authen')
const gen = require('./gen')
const fs = require('fs')


router.get('/list_payoff', async (req, res, next) => {
  try {
    let token  = req.headers.authorization
    let validate_token = authen.validate_token(token)
    let payer_tax_id = validate_token.source.sub
    var filter_admin = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401);
    } else {
      var where = "where po.payer_tax_id = '"+ payer_tax_id +"' and po.tranfer_type not like 'RD' "
      if(filter_admin == 'All'){
        where =  "where po.tranfer_type not like 'RD' "
      }
     
      var alltransaction = await db.sequelize.query(
        "select  po.payment_no, po.order_id, t.receiver_company, "+
        "        t.detail, po.receiver_account_name, "+
        "        case when po.status_code = '1' then 'Completed' "+
        "        when po.status_code = '000' then 'In process' "+
        "        else 'Rejected' end as transfer_processing, "+

        "        case when rd_po.status_code = '000' and w.process = 'validated' and w.status = 'success' then 'In process' "+
        "        when rd_po.status_code = '1' and (w.process = 'validated' or w.process = 'reconciled') and w.status = 'success'  then 'Completed' "+
        "        when rd_po.status_code = '2' then 'Rejected' "+
        "        else '' end as rd_processing , "+

        "        po.receiver_tax_id, po.receiver_account_no, b.name as receiver_bankcode, t.receiver_email,"+
        "        t.amount_exc_vat, t.vat_amount, t.wht_rate, t.wht_amount, "+ 
        "        (t.amount_exc_vat + t.vat_amount) as grand_total, po.pay_amount, "+
        "        po.payer_tax_id, pt.payment_date, pt.payment_time, "+
        "        case when w.income_type is not null then it_w.name_th else it_p.name_th end as income_type "+

        "from PayOffs  po "+
        "left join Transactions t on po.order_id = t.order_id "+
        "left join WHTs w on po.payment_no = w.payment_no "+
        "left join PayOffs  rd_po on w.rd_ref = rd_po.payment_no "+

        "left join Payment_Trackings pt on po.tracking_id = pt.id "+
        "left join Banks b on po.receiver_bank_code = b.code "+ 
        "left join Income_Types as it_p on it_p.code = t.income_type "+
        "left join Income_Types as it_w on it_w.code = w.income_type "+
        where +
        "order by pt.payment_date desc, po.payment_no asc ",
        {
          replacements: { payer_tax_id: payer_tax_id },
          type: QueryTypes.SELECT
        }
      )
      res.send(alltransaction)
    }
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})


router.post('/resend_payoff', async (req, res, next) => {
  try {
    let token  = req.headers.authorization
    let validate_token = authen.validate_token(token)
    let payer_tax_id = validate_token.source.sub
    if (validate_token.status === 'invalid token') {
      res.send(401);
    } else {
      // console.log('req.body')
      // console.log(req.body)
      var payoff_data = await PayOff.findOne({ where: { payment_no: req.body.payment_no } })
      var bank_data = await Bank.findOne({ where: { name: req.body.receiver_bank_code } })

      await Payoff_Log.create({
        payment_no: payoff_data.payment_no,
        receiver_account_no: payoff_data.receiver_account_no,
        receiver_account_name: payoff_data.receiver_account_name,
        receiver_bank_code: payoff_data.receiver_bank_code,
        receiver_branch_code: payoff_data.receiver_branch_code,
        createdBy: payoff_data.createdBy
      })
      await payoff_data.update({ 
        receiver_account_name: req.body.receiver_account_name,
        receiver_account_no: req.body.receiver_account_no,
        receiver_bank_code: bank_data.code,
        receiver_branch_code: '0'+req.body.receiver_account_no.substring(0, 3),
        status_code: '000',
        status_message: 'In process',
        createdBy: req.body.createBy
      })

      var pv_inprocess = await db.sequelize.query(
        " select * from PayOffs "+
        " where tracking_id = :tracking_id and (status_code = '000' or status_code = '1') and tranfer_type = 'Receiver' ",
        {
          replacements: { tracking_id: payoff_data.tracking_id },
          type: QueryTypes.SELECT
        }
      )
      // console.log('pv_inprocess : '+String(pv_inprocess.length))
      var pv_available = await db.sequelize.query(
        " select count(payment_no) as count from PayOffs "+
        " where tracking_id = :tracking_id and tranfer_type = 'Receiver' ",
        {
          replacements: { tracking_id: payoff_data.tracking_id },
          type: QueryTypes.SELECT
        }
      )
      // console.log('pv_available : '+String(pv_available[0].count))

      if (pv_inprocess.length == pv_available[0].count) {
        var receiver_data = []
        for (let data of pv_inprocess) {
          var data_receiver = {}
          Object.assign(data_receiver, {
            'transaction_ref': data.payment_no,
            'name': data.receiver_account_name,
            'tax_id': data.receiver_tax_id,
            'account_no': data.receiver_account_no,
            'bank_code': data.receiver_bank_code,
            'branch_code': data.receiver_branch_code,
            'amount': String(data.pay_amount)
          })
          receiver_data.push(data_receiver)
        }

        var sender_data = await Company.findOne()
        var direct_credit_data = {
        sender_info : {
          id: sender_data.company_id,
          name: sender_data.name,
          tax_id: sender_data.sender_tax_id,
          account_no: sender_data.bank_book_no,
          bank_code: sender_data.bank_code,
          branch_code: sender_data.branch_code
         },
          receiver_info: receiver_data   
        }
        // console.log('resend: directcredit')
        // console.log(direct_credit_data)
        try {
          var log_PayDC = await Log_PayDC.create({ 
            tracking_id: payoff_data.tracking_id, tranfer_type: 'Receiver', call_data: JSON.stringify(direct_credit_data) 
          })
          await axios.post(process.env.API_HOST+'/receiver_directcredit', direct_credit_data)
          res.send('Success')
        } catch (error) {
          console.log(error)
          var error_log_name = await gen.log_name()
          const error_console = new console.Console(fs.createWriteStream(error_log_name))
          error_console.log(error)
          await log_PayDC.update({ call_data: 'error' })
          res.send({status: 'Error: Receiver Direct Credit'}) 
        }

      }
      res.send('Success')
    }  
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    res.send({status: 'Error'}) 
  }
})

async function gen_no () {
  try {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();
    if(dd<10) { dd='0'+dd } 
    if(mm<10) { mm='0'+mm } 
    today = yyyy+mm+dd
    var payment_no = 'P'+today
    // console.log('payment_no')
    // console.log(payment_no)
    var data = await db.sequelize.query(
      'select TOP (1) payment_no ' +
      'from PayOffs ' +
      "where payment_no like '"+ payment_no  + "%'"+
      'order by id desc',
      {
        type: QueryTypes.SELECT
      }
    )   

    if (data.length > 0) {
      var index = String(parseInt(data[0].payment_no.substring(9)) + 1)
      while (index.length < 4) {
        index = '0' + index
      }
      payment_no = payment_no + index
    } else {
      payment_no = payment_no + '0001'
    }
    return ({ status: 'Success', payment_no: payment_no })
  } catch (error) {
    console.log(error)
    var error_log_name = await gen.log_name()
    const error_console = new console.Console(fs.createWriteStream(error_log_name))
    error_console.log(error)
    // res.send({status: 'Error'}) 
    return ({ status: 'Error gen_no' })
  }
}


module.exports = router
