require("dotenv").config();
const express = require("express");
var path = require('path')
var cors = require('cors')
const { Sequelize, DataTypes } = require('sequelize')

var admin = require('./routes/admin')
var adminManagement = require('./routes/admin_management')
var chat = require('./routes/chat')
var company = require('./routes/company')

var createPayment = require('./routes/createpayment')
var dashboard = require('./routes/dashboard')
var directCallback = require('./routes/direct_callback')
var ewhtCallback = require('./routes/ewht_callback')

var getTransaction = require('./routes/gettransaction')
var login = require('./routes/login')
var noti = require('./routes/notification')
var mainChart = require('./routes/main_chart')
var mainChartAdmin = require('./routes/main_chartadmin') 

var paymentCallback = require('./routes/payment_callback')
var payoff = require('./routes/payoff')
var rd = require('./routes/rd')
var sandbox = require('./routes/sandbox')

var tax = require('./routes/tax')
var upload = require('./routes/upload')

const app = express();
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))


app.use('/', [ admin, adminManagement, chat, company,
               createPayment, dashboard, directCallback, ewhtCallback,
               getTransaction, login, noti, mainChart,
               paymentCallback, payoff, rd, sandbox,
               tax, upload, mainChartAdmin
    ])


// # handel error methods
app.route('*').all((req, res) => {
  res.status(404).send({ status: 'fail', data: '', message: 'Method not found' })
})

// Start app
app.listen(process.env.PORT, () => console.log(`server has started at port ${process.env.PORT}`))
