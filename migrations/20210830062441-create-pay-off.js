'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PayOffs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      payment_no: {
        type: Sequelize.STRING
      },
      order_id: {
        type: Sequelize.STRING
      },
      tracking_id: {
        type: Sequelize.STRING
      },
      payer_tax_id: {
        type: Sequelize.STRING
      },
      receiver_tax_id: {
        type: Sequelize.STRING
      },
      receiver_account_name: {
        type: Sequelize.STRING
      },
      receiver_account_no: {
        type: Sequelize.STRING
      },
      receiver_bank_code: {
        type: Sequelize.STRING
      },
      receiver_branch_code: {
        type: Sequelize.STRING
      },
      pay_amount: {
        type: Sequelize.FLOAT
      },
      tranfer_type: {
        type: Sequelize.STRING
      },
      status_code: {
        type: Sequelize.STRING
      },
      status_message: {
        type: Sequelize.STRING
      },
      transaction_date: {
        type: Sequelize.STRING
      },
      transaction_time: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      status_payoff: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PayOffs');
  }
};