'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('WHTs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rd_ref: {
        type: Sequelize.STRING
      },
      payment_no: {
        type: Sequelize.STRING
      },
      payer_tax_id: {
        type: Sequelize.STRING
      },
      receiver_account_name: {
        type: Sequelize.STRING
      },
      receiver_account_no: {
        type: Sequelize.STRING
      },
      receiver_bank_code: {
        type: Sequelize.STRING
      },
      receiver_branch_code: {
        type: Sequelize.STRING
      },
      receiver_tax_id: {
        type: Sequelize.STRING
      },
      wht_amount: {
        type: Sequelize.FLOAT
      },
      wht_condition: {
        type: Sequelize.STRING
      },
      income_type: {
        type: Sequelize.STRING
      },
      process: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      transection_date: {
        type: Sequelize.STRING
      },
      transection_time: {
        type: Sequelize.STRING
      },
      editBy: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      status_wht: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('WHTs');
  }
};