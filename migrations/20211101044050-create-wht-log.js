'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Wht_Logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rd_ref: {
        type: Sequelize.STRING
      },
      payment_no: {
        type: Sequelize.STRING
      },
      receiver_tax_id: {
        type: Sequelize.STRING
      },
      wht_condition: {
        type: Sequelize.STRING
      },
      income_type: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Wht_Logs');
  }
};