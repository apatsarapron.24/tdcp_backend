'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Payment_Trackings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      merchant_id: {
        type: Sequelize.STRING
      },
      payer_tax_id: {
        type: Sequelize.STRING
      },
      payment_ref: {
        type: Sequelize.STRING
      },
      ops_payment_ref: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      bank_name: {
        type: Sequelize.STRING
      },
      payment_method: {
        type: Sequelize.STRING
      },
      pay_amount: {
        type: Sequelize.STRING
      },
      rcode: {
        type: Sequelize.STRING
      },
      payment_status: {
        type: Sequelize.STRING
      },
      payment_date: {
        type: Sequelize.STRING
      },
      payment_time: {
        type: Sequelize.STRING
      },
      payer: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Payment_Trackings');
  }
};