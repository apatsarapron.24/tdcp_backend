'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      order_id: {
        type: Sequelize.STRING
      },
      due_date: {
        type: Sequelize.STRING
      },
      detail: {
        type: Sequelize.STRING
      },
      payer_tax_id: {
        type: Sequelize.STRING
      },
      receiver_account_no: {
        type: Sequelize.STRING
      },
      receiver_account_name: {
        type: Sequelize.STRING
      },
      receiver_bank_code: {
        type: Sequelize.STRING
      },
      receiver_tax_id: {
        type: Sequelize.STRING
      },
      receiver_email: {
        type: Sequelize.STRING
      },
      amount_exc_vat: {
        type: Sequelize.FLOAT
      },
      vat_amount: {
        type: Sequelize.FLOAT
      },
      income_type: {
        type: Sequelize.STRING
      },
      wht_rate: {
        type: Sequelize.FLOAT
      },
      wht_amount: {
        type: Sequelize.FLOAT
      },
      wht_condition: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Transactions');
  }
};