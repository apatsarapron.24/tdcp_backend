'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Payoff_Logs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      payment_no: {
        type: Sequelize.STRING
      },
      receiver_account_no: {
        type: Sequelize.STRING
      },
      receiver_account_name: {
        type: Sequelize.STRING
      },
      receiver_bank_code: {
        type: Sequelize.STRING
      },
      receiver_branch_code: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Payoff_Logs');
  }
};