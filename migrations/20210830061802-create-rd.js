'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('RDs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tax_id: {
        type: Sequelize.STRING
      },
      account_no: {
        type: Sequelize.STRING
      },
      account_name: {
        type: Sequelize.STRING
      },
      bank_code: {
        type: Sequelize.STRING
      },
      branch_code: {
        type: Sequelize.STRING
      },
      editedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('RDs');
  }
};