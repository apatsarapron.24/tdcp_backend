'use strict';
module.exports = (sequelize, DataTypes) => {
  const Payer = sequelize.define('Payer', {
    tax_id: DataTypes.STRING,
    name_th: DataTypes.STRING,
    name_eng: DataTypes.STRING
  }, {});
  Payer.associate = function(models) {
    // associations can be defined here
  };
  return Payer;
};