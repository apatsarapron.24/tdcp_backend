'use strict';
module.exports = (sequelize, DataTypes) => {
  const WHT = sequelize.define('WHT', {
    rd_ref: DataTypes.STRING,
    payment_no: DataTypes.STRING,
    payer_tax_id: DataTypes.STRING,
    receiver_tax_id: DataTypes.STRING,

    wht_amount: DataTypes.FLOAT,
    wht_condition: DataTypes.STRING,
    income_type: DataTypes.STRING,
    process: DataTypes.STRING,

    status: DataTypes.STRING,
    description: DataTypes.STRING,
    validate_transdate: DataTypes.STRING,
    validate_transtime: DataTypes.STRING,

    reconclie_transdate: DataTypes.STRING,
    reconclie_transtime: DataTypes.STRING,
    createdBy: DataTypes.STRING
  }, {});
  WHT.associate = function(models) {
    // associations can be defined here
  };
  return WHT;
};