'use strict';
module.exports = (sequelize, DataTypes) => {
  const Log_RDdata = sequelize.define('Log_RDdata', {
    tracking_id: DataTypes.STRING,
    data_type: DataTypes.STRING,
    call_data: DataTypes.STRING,
    callback_data: DataTypes.STRING
  }, {});
  Log_RDdata.associate = function(models) {
    // associations can be defined here
  };
  return Log_RDdata;
};