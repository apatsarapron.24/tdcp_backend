'use strict';
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    order_id: DataTypes.STRING,
    doc_date: DataTypes.STRING,
    detail: DataTypes.STRING,
    payer_tax_id: DataTypes.STRING,
    receiver_company: DataTypes.STRING,
    receiver_tax_id: DataTypes.STRING,
    receiver_account_no: DataTypes.STRING,
    receiver_account_name: DataTypes.STRING,
    receiver_bank_code: DataTypes.STRING,
    receiver_type: DataTypes.STRING,
    receiver_mobile: DataTypes.STRING,
    receiver_email: DataTypes.STRING,
    amount_exc_vat: DataTypes.FLOAT,
    vat_amount: DataTypes.FLOAT,
    income_type: DataTypes.STRING,
    wht_rate: DataTypes.FLOAT,
    wht_amount: DataTypes.FLOAT,
    wht_condition: DataTypes.STRING,
    createdBy: DataTypes.STRING
  }, {});
  Transaction.associate = function(models) {
    // associations can be defined here
  };
  return Transaction;
};