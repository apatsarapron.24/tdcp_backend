'use strict';
module.exports = (sequelize, DataTypes) => {
  const Company = sequelize.define('Company', {
    company_id: DataTypes.STRING,
    sender_tax_id: DataTypes.STRING,
    bank_book_no: DataTypes.STRING,
    bank_code: DataTypes.STRING,
    branch_code: DataTypes.STRING,
    name: DataTypes.STRING
  }, {});
  Company.associate = function(models) {
    // associations can be defined here
  };
  return Company;
};