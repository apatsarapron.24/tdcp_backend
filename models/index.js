'use strict'
const dotenv = require('dotenv')
const fs = require('fs')
const path = require('path')
const filebasename = path.basename(__filename)
const { Sequelize, DataTypes } = require('sequelize')
const db = {}

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mssql',
    port: 6433,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    timezone : "+7.00"
})

// console.log('__dirname: ', __dirname)
fs
  .readdirSync(__dirname)
  .filter((file) => {
  const returnFile = (file.indexOf('.') !== 0) && (file !== filebasename) && (file.slice(-3) === '.js')
  return returnFile
})

.forEach((file) => {
  // console.log('file: ', file)
  const model = require(__dirname+'/'+file)(sequelize, Sequelize.DataTypes);
  db[model.name] = model;
})

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

// const sequelizeOptions = { logging: console.log, }
// sequelize.sync(sequelizeOptions)
// .catch((err) => {
// console.log(err);
// process.exit();
// });

module.exports = db;