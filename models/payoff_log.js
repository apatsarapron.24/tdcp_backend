'use strict';
module.exports = (sequelize, DataTypes) => {
  const Payoff_Log = sequelize.define('Payoff_Log', {
    payment_no: DataTypes.STRING,
    receiver_account_no: DataTypes.STRING,
    receiver_account_name: DataTypes.STRING,
    receiver_bank_code: DataTypes.STRING,
    receiver_branch_code: DataTypes.STRING,
    createdBy: DataTypes.STRING
  }, {});
  Payoff_Log.associate = function(models) {
    // associations can be defined here
  };
  return Payoff_Log;
};