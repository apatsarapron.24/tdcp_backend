'use strict';
module.exports = (sequelize, DataTypes) => {
  const Income_Type = sequelize.define('Income_Type', {
    code: DataTypes.STRING,
    name_th: DataTypes.STRING
  }, {});
  Income_Type.associate = function(models) {
    // associations can be defined here
  };
  return Income_Type;
};