'use strict';
module.exports = (sequelize, DataTypes) => {
  const Log_PayDC = sequelize.define('Log_PayDC', {
    tracking_id: DataTypes.STRING,
    tranfer_type: DataTypes.STRING,
    call_data: DataTypes.STRING,
    callback_data: DataTypes.STRING
  }, {});
  Log_PayDC.associate = function(models) {
    // associations can be defined here
  };
  return Log_PayDC;
};