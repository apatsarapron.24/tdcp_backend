'use strict';
module.exports = (sequelize, DataTypes) => {
  const PayOff = sequelize.define('PayOff', {
    payment_no: DataTypes.STRING,
    order_id: DataTypes.STRING,
    tracking_id: DataTypes.STRING,
    payer_tax_id: DataTypes.STRING,

    receiver_tax_id: DataTypes.STRING,
    receiver_account_name: DataTypes.STRING,
    receiver_account_no: DataTypes.STRING,
    receiver_bank_code: DataTypes.STRING,

    receiver_branch_code: DataTypes.STRING,
    pay_amount: DataTypes.FLOAT,
    tranfer_type: DataTypes.STRING,
    status_code: DataTypes.STRING,

    status_message: DataTypes.STRING,
    transaction_date: DataTypes.STRING,
    transaction_time: DataTypes.STRING,
    createdBy: DataTypes.STRING
  }, {});
  PayOff.associate = function(models) {
    // associations can be defined here
  };
  return PayOff;
};