'use strict';
module.exports = (sequelize, DataTypes) => {
  const RD = sequelize.define('RD', {
    tax_id: DataTypes.STRING,
    account_no: DataTypes.STRING,
    account_name: DataTypes.STRING,
    bank_code: DataTypes.STRING,
    branch_code: DataTypes.STRING,
    createdBy: DataTypes.STRING
  }, {});
  RD.associate = function(models) {
    // associations can be defined here
  };
  return RD;
};