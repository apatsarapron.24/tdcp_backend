'use strict';
module.exports = (sequelize, DataTypes) => {
  const Transaction_Payment = sequelize.define('Transaction_Payment', {
    tracking_id: DataTypes.STRING,
    order_id: DataTypes.STRING
  }, {});
  Transaction_Payment.associate = function(models) {
    // associations can be defined here
  };
  return Transaction_Payment;
};