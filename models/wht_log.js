'use strict';
module.exports = (sequelize, DataTypes) => {
  const Wht_Log = sequelize.define('Wht_Log', {
    rd_ref: DataTypes.STRING,
    payment_no: DataTypes.STRING,
    receiver_tax_id: DataTypes.STRING,
    wht_condition: DataTypes.STRING,
    income_type: DataTypes.STRING,
    createdBy: DataTypes.STRING
  }, {});
  Wht_Log.associate = function(models) {
    // associations can be defined here
  };
  return Wht_Log;
};