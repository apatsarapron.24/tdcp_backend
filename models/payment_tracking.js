'use strict';
module.exports = (sequelize, DataTypes) => {
  const Payment_Tracking = sequelize.define('Payment_Tracking', {
    merchant_id: DataTypes.STRING,
    payer_tax_id: DataTypes.STRING,
    call_access_token: DataTypes.STRING,
    callback_access_token: DataTypes.STRING,
    call_page_bank: DataTypes.STRING,
    callback_page_bank: DataTypes.STRING,
    payment_ref: DataTypes.STRING,
    ops_payment_ref: DataTypes.STRING,
    description: DataTypes.STRING,
    bank_name: DataTypes.STRING,
    payment_method: DataTypes.STRING,
    pay_amount: DataTypes.STRING,
    rcode: DataTypes.STRING,
    payment_status: DataTypes.STRING,
    payment_date: DataTypes.STRING,
    payment_time: DataTypes.STRING,
    payer: DataTypes.STRING
  }, {});
  Payment_Tracking.associate = function(models) {
    // associations can be defined here
  };
  return Payment_Tracking;
};